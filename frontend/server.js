const path = require('path');
const express = require('express');
const server = express();
const webpack = require('webpack')
const config = require('./config/webpack.config.js');
const compiler = webpack(config);

const webpackDevMiddleware = require('webpack-dev-middleware')(
  compiler,
  config.devServer
)

const expressStaticGzip = require('express-static-gzip');
const webpackHotMiddleware = require('webpack-hot-middleware')(compiler);

server.use(webpackDevMiddleware);
server.use(webpackHotMiddleware);
server.use(expressStaticGzip('dist', {
  enableBrotli: true
}))

console.log(__dirname)

// Handles the routing
server.get('*', (req,res) =>{
  res.sendFile(path.join(__dirname+'/dist/index.html'));
});

const PORT = process.env.PORT || 8080
server.listen(PORT, () => {
  console.log(`the server is listening in ${PORT}`)
})