import React, { Component } from 'react';
import './style/dashboard.styl';
import { Button, Form, CommentActions } from 'semantic-ui-react';
import { GET_USER_INFO } from './queries/queries';
import { Query } from 'react-apollo';
import { cookieParser } from '../helpers/cookies';
import { Switch, Route, Redirect } from 'react-router-dom';
import { valueToObjectRepresentation } from 'apollo-utilities';

import Posts from './Posts.jsx';
import LoginHelper from './LoginHelper.jsx';
import Settings from './Settings.jsx';
import StripeTransactions from './StripeTransactions.jsx';
import PostHistory from './PostHistory.jsx';

require('dotenv').config();

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: '',
      editSettings: false,
      welcomeMessage: true,
      billing: false,
      isLoading: true,
      posts: false,
      history: false
    };

    this.app = this.props.app;
    this.userService = this.app.service('users');
    this.loginService = this.app.service('login');

    if (this.props.isLoggedIn) {
      this.isLoggedIn = this.props.isLoggedIn;
    }

    this.handleLogout = this.handleLogout.bind(this);
    this.handleBilling = this.handleBilling.bind(this);
    this.checkVerifiedStatus = this.checkVerifiedStatus.bind(this);
    this.handleToken = this.handleToken.bind(this);
    this.resetAll = this.resetAll.bind(this);
  }

  componentDidMount() {
    let pathname = window.location.pathname;
    switch (true) {
      case pathname.includes('history'):
        this.setState({
          billing: false,
          editSettings: false,
          welcomeMessage: true,
          posts: false,
          history: true,
        });
        break;
      case pathname.includes('post'):
        this.setState({
          billing: false,
          editSettings: false,
          welcomeMessage: true,
          posts: true,
          history: false,
        });
        break;
      case pathname.includes('settings'):
        this.setState({
          billing: false,
          editSettings: true,
          welcomeMessage: false,
          posts: false,
          history: false,
        });
        break;
    }
  }

  clearCookies(keyName = null) {
    let expireDate = new Date();
    expireDate.setTime(expireDate.getTime() - 1);

    if (keyName) {
      document.cookie = `${keyName}=; expires=${expireDate.toUTCString()};Path=/;`;
    } else {
      const cookies = document.cookie.split(';');

      cookies.forEach((value) => {
        document.cookie = value.replace(/^ +/, '').replace(/=.*/, '=;expires=' + expireDate.toUTCString());
      });
    }
  }

  async checkVerifiedStatus() {
    this.userService
      .get(this.userId)
      .then((response) => {
        // check if user is verified.
        this.isVerified = response.isVerified;
      })
      .then(() => {
        // if user is verified return
        if (this.isVerified) {
          return;
        }
        // otherwise throw an error message.
        this.setState({
          errorMessage: 'Please verify your email address!'
        });
      });
  }

  userInfoCollect() {
    //After logging in it saves user's information. Only writes if hour is different to avoid flooding.
    const expire = sessionStorage.getItem('expireTime');

    if (!expire) {
      this.loginService
        .create({
          'belongsTo': this.userId,
          'userAgent': navigator.userAgent
        }).then((response) => {
          sessionStorage.setItem('expireTime', new Date());
        })
    }
  }

  async handleToken(token) {
    let userId;

    await this.app.passport.verifyJWT(token)
      .then(response => {
        userId = response.userId;
      });

    return userId;
  }

  handleLogout(e) {
    e.preventDefault();
    this.setState({
      isLoading: true
    });

    if (this.token) {
      // remove token cookie
      this.clearCookies('token');
      this.token = null;
      window.location = '/dashboard';
    } else {
      // clear authentication key
      const app = this.app;
      app.logout();

      //clear localStorage
      localStorage.clear();
      //clear sessionStorage
      sessionStorage.clear();

      //clear cookies
      this.clearCookies();
      window.location = '/';
    }

    this.isLoggedIn = false;

    this.setState({
      isLoading: false
    });
  }

  handleBilling() {
    this.setState({
      billing: true,
      editSettings: false,
      welcomeMessage: false,
      posts: false,
      history: false
    });
  }

  resetAll() {
    this.setState({
      billing: false,
      editSettings: false,
      welcomeMessage: true,
      posts: false,
      history: false
    });
  }

  // TODO: Need to check if user is actually logged in, or if their token has expired,
  // through the server as well for double check, this needs to be done in the background for better user experience.
  // After displaying the reason, then redirect to login page. Set isLoggedIn state to false.
  componentWillMount() {
    const cookieData = document.cookie.split(';');

    this.app.service('cookies')
      .patch('', {
        keyName: 'token',
        cookieData
      })
      .then(response => {
        if (response === 'error') {

          cookieParser('userId', this.app)
            .then(response => {
              if (response) {
                this.userId = response;
                this.isLoggedIn = true;
                this.userInfoCollect();

                this.setState({
                  isLoading: false
                })
              }
            });

        } else {
          this.token = response;
        }
      })
      .then(() => {
        if (this.token) {
          this.setState({
            errorMessage: '',
            editSettings: false,
            welcomeMessage: true,
            billing: false,
            isLoading: true
          });

          this.handleToken(this.token)
            .then(userId => {
              this.userId = userId;
              this.isLoggedIn = true;

              this.setState({
                isLoading: false
              })
            });
        }
      })
  }

  render() {
    const { userId, isLoggedIn, token } = this;
    let uCool = false;
    let isPremium;
    const {
      welcomeMessage,
      errorMessage,
      editSettings,
      billing,
      posts,
      history,
      isLoading
    } = this.state;

    if (isLoading) {
      return (
        <div>Loading...</div>
      );
    }

    if (isLoggedIn || token) {
      return (
        <Query query={GET_USER_INFO} variables={{ userId: userId }}>
          {({ loading, error, data }) => {
            if (loading) return 'loading...';
            if (error) return <Button onClick={this.handleLogout}>Logout</Button>;
            let userInfo;
            let role = data.users_by_pk.role;
            if (role == process.env.ROLE) {
              uCool = true;
            }
            // if the data exists, try to get the user profile.
            if (data.users_by_pk.userProfile) {
              for (let individual in data.users_by_pk.userProfile) {
                userInfo = data.users_by_pk.userProfile[individual];
                isPremium = (userInfo.role && userInfo.role !== 'user') ? '| ' + userInfo.role + ' member' : null;
              }
            }

            return (
              <div>
                <div className="dashboard-container">
                  <div className="left-sidebar">
                    <a href="/dashboard">

                      <button onClick={this.resetAll}>Dashboard</button></a>

                    <a href="/dashboard/post"><button className="button">
                      Create a Post
                    </button></a>

                    <a href="/dashboard/settings">
                    <button className="button">
                    
                      Settings
                    </button></a>
                    <a href="/dashboard/history"><button className="button">
                      history
                    </button></a>

                    <button onClick={this.handleBilling} className="button">
                      Billing
                    </button>

                    {uCool ? (<a href="/pirates/" ><button className="button">Admin</button></a>) : ('')}

                    <Button onClick={this.handleLogout}>Logout</Button>
                  </div>
                  <div className="right-content">

                    {errorMessage && (
                      // form error messages are displayed here only when there is an error message.
                      <div className="error-message">{errorMessage}</div>
                    )}

                    {welcomeMessage && (
                      // if there is a welcome message display it
                      <div>
                        {userInfo ? (
                          <p>Welcome {userInfo.displayName || data.users_by_pk.user} {isPremium}</p> 
                        ) : (
                            <p>Welcome {data.users_by_pk.user}</p>
                          )}
                      </div>
                    )}


                    {editSettings && (
                      <Settings
                        userService={this.userService}
                        isLoggedIn={isLoggedIn}
                        app={this.app}
                        token={token}
                        userId={userId}
                      />
                    )}

                    {posts &&
                      <Posts
                        app={this.app}
                        userID={this.userId}
                      />}
                    {history &&
                      <PostHistory
                        app={this.app}
                        userID={this.userId}
                      />}
                    {billing &&
                      <StripeTransactions
                        isLoggedIn={isLoggedIn}
                        app={this.app}
                        token={token}
                        userId={userId}
                      />}
                  </div>
                </div>
              </div>
            );
          }}
        </Query>
      );
    }

    return <LoginHelper />;
  }
}

export default Dashboard;
