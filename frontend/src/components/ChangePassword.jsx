import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';
import LoginHelper from './LoginHelper.jsx';

class ChangePassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      errorMessage: '',
      successMessage: ''
    }

    this.app = this.props.app;
    this.isLoggedIn = false;
    this.userId;
  
    this.checkAuth = this.checkAuth.bind(this);
    this.handlePwdChange = this.handlePwdChange.bind(this);
    this.verifyJWT = this.verifyJWT.bind(this);

    this.token = this.props.token ? this.props.token : '';

    this.checkAuth(this.token);
  }

  // check authentication before displaying form
  checkAuth(token = '') {
    if(!token) {
      this.app.authenticate().then((response) => {
        if(response.accessToken.length > 0) {
          // set login status to true if there is a token
          this.verifyJWT(response.accessToken);
        }   
      }).catch(() => {
        this.setState({
          isLoading: false
        });
      });
    } else {
      this.verifyJWT(token);
    }
  }

  verifyJWT(token) {
    this.app.passport
      .verifyJWT(token)
      .then(response => {
        this.userId = response.userId;
      })

    this.isLoggedIn = true;

    // set loading state to false
    this.setState({
      isLoading: false
    });
  }

  handlePwdChange(e) {
    e.preventDefault();
    const oldPassword = document.changePassword.oldPassword.value;
    const newPassword = document.changePassword.newPassword.value;
    const confirmPassword = document.changePassword.confirmPassword.value;

    if(newPassword === confirmPassword) {
      this.app.service('pwdhandler')
        .update(this.userId, {
          oldPassword,
          password: newPassword
        })
        .then(response => {
          if(response === 'success') {
            this.setState({
              errorMessage: '',
              successMessage: 'Password changed successfully.'
            })  
          } else {
            this.setState({
              errorMessage: response,
              successMessage: ''
            })  
          }
        })
        .catch(err => {
          console.log(err);
          this.setState({
            errorMessage: err.message,
            successMessage: ''
          })
        })
      } else {
        this.setState({
          errorMessage: `new passwords don't match`,
          successMessage: ''
        })
      }
  }

  render() { 
    const isLoggedIn = this.isLoggedIn;
    const { isLoading, successMessage, errorMessage } = this.state;

    if(isLoggedIn) {
      return (
        <div>
          <h2>Change Password</h2>
  
          <Form name="changePassword" onSubmit={this.handlePwdChange}>
            <Form.Field>
              <label htmlFor="oldPassword">Enter Password</label>
              <input id="oldPassword" type="password" name="password" placeholder="enter password" />
            </Form.Field>
  
            <Form.Field>
              <label htmlFor="newPassword">Enter New Password</label>
              <input id="newPassword" type="password" name="password" placeholder="enter password" />
            </Form.Field>
  
            <Form.Field>
              <label htmlFor="confirmPassword">Re-Confirm Password</label>
              <input id="confirmPassword" type="password" name="password" placeholder="enter password" />
            </Form.Field>
            <Button>Update Password</Button>
          </Form>
          
          {successMessage && 
            // form success messages are displayed here only when there is a success message.
            <div className="success-message">
              {successMessage}
            </div>
            }

            {errorMessage && 
            // form error messages are displayed here only when there is an error message.
            <div className="error-message">
              {errorMessage}
            </div>
            }
        </div>
      );  
    }

    if(isLoading) {
      return (
        <div>Loading...</div>
      )
    }

    return(
      <LoginHelper app={this.app} />
    )
  }
}
 
export default ChangePassword;