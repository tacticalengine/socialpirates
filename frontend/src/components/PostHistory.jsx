import React, { Component } from 'react';
import { GET_POSTS } from './queries/queries';
import { Query, Mutation } from 'react-apollo';
import { adopt } from 'react-adopt';
import moment from 'moment';
import Calendar from 'react-calendar/dist/entry.nostyle';
import './style/history.styl';

class PostHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userID: this.props.userID,
      currentDate: new Date(),
      dateCollection: [],
      postIDs: [],
      selectedDate: '',
      selectedMonth: '',
      selectedYear: '',
      postCount: 0
    }
    this.theDate = this.theDate.bind(this);
    this.handleDayColors = this.handleDayColors.bind(this);
    this.onDayClick = this.onDayClick.bind(this);
    this.monthYear = this.monthYear.bind(this);
  }


  componentDidMount() {
    console.log(this.state.postCount)
    //updates the tile after load
    return this.setState({
    }, () => {
      this.handleDayColors = this.handleDayColors.bind(this);
    })

  }

  onDayClick(date) {
    let dateNow = date
    let splitme = dateNow.toString().split(" ")
    let newDate = `${splitme[1]} ${splitme[2]}, ${splitme[3]}`;
    let newList = this.state.dateCollection;
    let theList = [];
    
    newList.map((item, key) => {
      if (item.createdAt == newDate) {
        theList.push(item.postID);
      }
    })
    //get rid of duplicates
    this.state.postIDs = [...new Set(theList)];
    return this.setState(
      {
        selectedDate: newDate
      }, () => {
        this.handleDayColors = this.handleDayColors.bind(this);
      })

  }

  handleDayColors({ date }) {
    let dateNow = date
    let splitme = dateNow.toString().split(" ")
    let newDate = `${splitme[1]} ${splitme[2]}, ${splitme[3]}`;
    let newList = this.state.dateCollection;
    let kitty;
    newList.map((item, key) => {
      if (item.createdAt == newDate) {
        kitty = 'ithas';
      }
    })
    return kitty;
  }
  monthYear(e) {
    let date = e.label.split(" ")
    let month = moment().month(date[0]).format('MM')
    this.state.selectedMonth = month;
    this.state.selectedYear = date[1];
    return e.label;
  }

  theDate(e) {
    //add the dates to state
    let theDate = moment.utc(e.createdAt)
    let formatted = {
      postID: e.id,
      createdAt: theDate.format("MMM DD, YYYY")}
    this.state.dateCollection.push(formatted);
    return formatted.createdAt + ' | ' + theDate.fromNow()
  }

  render() {
    let postNumber;
    let query = {
      'id': (this.state.postIDs.length > 0) ? { '_in': this.state.postIDs} : {}, 
      'belongsTo': {
        '_eq': this.state.userID
      },
      'createdAt': {
        '_ilike': `%${this.state.selectedYear}-${this.state.selectedMonth}%`
      }
  }
    const Composed = adopt({
      thePosts: ({ render }) => (
        <Query query={GET_POSTS} variables={{ postIDs: query, limit: 30 }}>
          {(data, loading, error) => render({ data, loading, error })}
        </Query>
      )
    })

    return (
      <div className="historyContainer">
        <Composed>
          {({ thePosts: { data, loading, error } }) => {
            if (loading) return 'loading';
            if (error) return 'there was something that went wrong';
            let allPosts;

            if (data.data) {
              for (let individual in data.data) {
                let posts = data.data[individual];
                this.state.postCount = posts.length;
                if (posts.length >= 1) {
                  postNumber = (this.state.postIDs.length > 0) ? `You have ${this.state.postIDs.length} posts on ${this.state.selectedDate}` : `Your last ${posts.length} posts`;
                  allPosts = posts.map((item, key) =>
                    <span className="posts" key={item.id}>
                      <span>{item.content}</span>
                      <span className="time">{this.theDate(item)}</span>
                    </span>
                  )
                } else {
                  allPosts = 'You have no posts';
                }
              }
            }
            return (
              <div>
                <h2>Calendar</h2>
                <Calendar navigationLabel={this.monthYear} tileClassName={this.handleDayColors} onChange={this.onDayClick} value={this.state.currentDate} />
                {postNumber ? (<h2>{postNumber}</h2>) : ('')}
                {allPosts}
              </div>
            )
          }}
        </Composed>
      </div>
    )

  }
}

export default PostHistory;