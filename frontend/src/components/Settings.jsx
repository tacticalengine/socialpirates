import React, { Component } from 'react';
import { cookieParser } from '../helpers/cookies';
import {timezones} from '../helpers/timezones.js';
import './style/settings.styl';
import PhoneVerification from './PhoneVerification.jsx';
import { Button, Form } from 'semantic-ui-react';
import ChangePassword from './ChangePassword.jsx';

class Settings extends Component {
	constructor(props) {
		super(props);
		// get the database connection and already connected userService passed from the Dashboard app
		this.app = this.props.app;
		this.userService = this.props.userService;

		this.state = {
			modal: false,
			twofa: false,
			changePassword: false,
			profileID: null,
			displayName: 'name',
			timezone: 'America/Los_Angeles',
			isLoading: false
		};

		this.displayName = React.createRef();
		this.timezone = React.createRef();
		this.success = React.createRef();
		this.saveUserInfo = this.saveUserInfo.bind(this);
		this.handle2FA = this.handle2FA.bind(this);
		this.handleChangePassword = this.handleChangePassword.bind(this);
		this.check2FAStatus = this.check2FAStatus.bind(this);
		this.profileService = this.app.service('profile');
		this.userId = this.props.userId;
		this.removeSuccess = this.removeSuccess.bind(this);
	}

	componentDidMount() {
		this._isMounted = true;
		// if a token exists, use the userId from the props instead
		if(this.props.token) {

			this.token = this.props.token;
			this.isLoggedIn = true;

			this.check2FAStatus();
		} else {
			cookieParser('userId', this.app)
			.then(response => {
				if(response) {
					this.userId = response;
					this.isLoggedIn = true;
					this.check2FAStatus();
				}
			})
		}
		this.profileService
		.find({
			query: {
				belongsTo: {
					$eq: this.userId
				}
			}
		}).then((response) => {

			if (response.data.length == 1) {
				for (let x of response.data) {
					this.setState({
						profileID: x.profileID,
					});
					if (x.displayName) {
						this.setState({
						displayName: x.displayName
					})
					}
					if (x.timezone) {
						this.setState({
							timezone: x.timezone
						})
					}
				}
			}
		})
	}
	removeSuccess() {
		this.success.current.innerHTML = ''
		this.success.current.className = ''
	}
	
	check2FAStatus() {
		// check two factor authentication status
		this.userService.get(this.userId)
		.then(response => {
			this.setState({
				twofa: response.twofa
			})
		})
	}

	handleChangePassword() {
		this.setState({
			changePassword: true
		})
	}

	handle2FA() {
		if(this.state.twofa === true) {
			this.userService.patch(this.userId, {
				twofa: false
			}).then(() => {
				// reset state
				this.setState({
					twofa: false
				})
			})
		} else {
			this.setState({
				modal: true
			})
		}
	}

	saveUserInfo(e) {
		let nameValue = this.displayName.current.value || this.state.displayName;
		let timeValue = this.timezone.current.value;
		this.setState({
			displayName: nameValue
		});
		this.profileService.patch(this.state.profileID, {
			'belongsTo': this.userId,
			'displayName': nameValue,
			'timezone': timeValue
		}).then(_ => {
				this.displayName.current.value = '';
				this.success.current.className = 'submit-success';
				this.success.current.innerHTML = 'Your settings have been changed!';

		}).catch((err) => console.log(err))

	}

	componentWillUnmount() {
		this._isMounted = false;
	}

	render() {
		let isLoggedIn = this.isLoggedIn;
		let { modal, twofa, changePassword } = this.state;
		const listTimezones = timezones.map((key) => <option key={key} >{key}</option>)

		if(changePassword) {
			return (
				<ChangePassword 
					app={this.app}
					token = {this.token ? this.token : ''}
					userId = {this.userId ? this.userId : ''}
				 />
			)
		}

		if(modal) {
			return (
				<PhoneVerification 
					userService={ this.userService ? this.userService : '' } 
					isLoggedIn={this.isLoggedIn} 
					app={this.app}
					token = {this.token ? this.token : ''}
					userId = {this.userId ? this.userId : ''}
				/>
			)
		}

		if(isLoggedIn) {
			return (
				<div>
								<Form className="settings-form"
									onSubmit={this.saveUserInfo} >
										<Form.Field>
											<label htmlFor="displayName">Name</label>
											<input ref={this.displayName} id="displayName" placeholder={this.state.displayName } />
										</Form.Field>
										<Form.Field>
											<label htmlFor="timezone">Timezone</label>
											<select ref={this.timezone} id="timezone" value={this.state.timezone} onChange={e => this.setState({timezone: e.target.value})} >
												{listTimezones}
										</select>
										</Form.Field>
									<Button>Submit</Button>
								</Form>
								<div ref={this.success} onClick={this.removeSuccess}></div>
					<div>
						{twofa ? (
							<div>
								<label htmlFor="2faDisable">2 Factor Authentication</label>
								<button id="2faDisable" onClick={this.handle2FA}>Disable</button>
							</div>
						) : (
							<div>
								<label htmlFor="2faEnable">2 Factor Authentication</label>
								<button id="2faEnable" onClick={this.handle2FA}>Enable</button>
							</div>
						)}
							<div>
								<button onClick={this.handleChangePassword}>Change Password</button>
							</div>
					</div>
				</div>
			)
		
		}
			return ('');
		

	}
}

export default Settings;