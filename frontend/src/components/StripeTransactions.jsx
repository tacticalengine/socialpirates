import React, { Component } from 'react';
import { cookieParser } from '../helpers/cookies';
import moment from 'moment';

import LoginHelper from './LoginHelper.jsx';

class StripeTransactions extends Component {
  constructor(props) {
    super(props);
  
    this.isLoggedIn = this.props.isLoggedIn ? this.props.isLoggedIn : false
    this.app = this.props.app;
  }

  componentWillMount() {
    this.billingData;

    this.setState({
      isLoading: true
    })

    if(this.props.token) {
      this.userId = this.props.userId;
    
      this.getBillingDetails();
    } else {
      cookieParser('userId', this.app)
      .then(response => {
        this.userId = response;

        this.getBillingDetails();
      })
    }
  }

  async getBillingDetails() {   
    await this.app.service('users')
      .get(this.userId)
      .then(response => {
        const customerId = response.stripe_cust_ID ? response.stripe_cust_ID : null;

        if(customerId) {
          this.app.service('handler')
          .find({
            query: {
              customerId
            }
          })
          .then(response => {
            this.billingData = response.data;
            this.setState({
              isLoading: false
            })
          })  
        } else {
          this.billingData = null;
          
          this.setState({
            isLoading: false
          })
        }
      })   
  }

  render() {
    const { isLoggedIn, billingData } = this;
    const isLoading = this.state.isLoading;

    if(isLoading) {
      return (
        <div>loading...</div>
      )
    }

    if(billingData === null && isLoggedIn) {
      return(
        <div>
          <h2>Billing History</h2>
          <p>You have no transaction history.</p>
        </div>
      )
    }

    if(isLoggedIn) {
      return (
        <div>
        <h2>Billing History</h2>
        <table className="data-table">
          <tbody>
            <tr>
              <th>Date Created</th>
              <th>Amount</th>
              <th>Status</th>
            </tr>
          {billingData.map(data => {
            let date = moment(data.created * 1000).fromNow();
            return ( 
              <tr key={data.created}>
                <td>{date}</td>
                <td>${data.amount/100}</td>
                <td>{data.refunded ? 'Refunded' : data.status}</td>
              </tr>
              )
            })}
            </tbody>
          </table>
        </div>
      );
    }

    return (
      <LoginHelper app={this.app} />
    )
  }
}
 
export default StripeTransactions;