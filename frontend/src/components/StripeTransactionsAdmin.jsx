import React, { Component } from 'react';
import moment from 'moment';

import LoginHelper from './LoginHelper.jsx';

class StripeTransactions extends Component {
  constructor(props) {
    super(props);
  
    this.userId = this.props.match.params ? this.props.match.params.id : null;
    this.isLoggedIn = this.props.isLoggedIn ? this.props.isLoggedIn : false
    this.app = this.props.app;
    this.isCool = false;
    this.billingData;

    this.getBillingDetails = this.getBillingDetails.bind(this);
    this.handleRefund = this.handleRefund.bind(this);

    if(this.userId) {
      this.state = {
        isLoading: true
      }
      this.getBillingDetails();  
    } else {
      this.state = {
        isLoading: false,
        errorMessage: 'Error: No user ID provided.'
      }
    }
  }

  componentWillMount() {
    this.app.authenticate()
    .then(response => {
      this.isLoggedIn = true;
      this.app.passport
      .verifyJWT(response.accessToken)
      .then(response => {
        this.app.service('users')
        .get(response.userId)
        .then(userData => {
          if(userData.role === process.env.ROLE) {
            this.isCool = true;
          }
        })
      })
    })
    .catch(() => {
      this.isLoggedIn = false;
    })
  }

  async getBillingDetails() {
    await this.app.service('users')
    .get(this.userId)
    .catch(err => {
      if(err.message === 'No auth token') {
        this.setState({
          isLoading: false,
          loginHelperMessage: {
            content: 'You need to login first.',
            success: false
          }
        })
      } else {
        this.setState({
          isLoading: false,
          errorMessage: err.message
        })
      }
    })
    .then(response => {
      if(response) {
        const customerId = response.stripe_cust_ID ? response.stripe_cust_ID : null;

        if(customerId) {
          this.app.service('handler')
          .find({
            query: {
              customerId
            }
          })
          .then(response => {
            this.billingData = response.data;
            this.setState({
              isLoading: false
            })
          })  
        } else {
          this.billingData = null;
          
          this.setState({
            isLoading: false
          })
        }
      }
    })
  }

  handleRefund(chargeId) {
    this.app.service('handler')
    .update('', {chargeId})
    .then(response => {
      if(response) {
        this.getBillingDetails();   
      }
    })
  }

  render() {
    const { isLoggedIn, billingData, isCool } = this;
    const { isLoading, errorMessage, loginHelperMessage } = this.state;

    if(isLoading) {
      return (
        <div>loading...</div>
      )
    }

    if(errorMessage) {
      return (
        <div>{errorMessage}</div>
      )
    }

    if(billingData === null && isLoggedIn) {
      return(
        <div>
          <h2>Billing History</h2>
          <p>You have no transaction history.</p>
        </div>
      )
    }

    if(isLoggedIn && isCool) {
      return (
      <div>
        <h2>Billing History</h2>
        <table className="data-table">
          <tbody>
            <tr>
              <th>Date Created</th>
              <th>Amount</th>
              <th>Status</th>
              <th></th>  
            </tr>
        {billingData.map(data => {
          let date = moment(data.created * 1000).fromNow();
          return ( 
            <tr key={data.created}>
              <td>{date}</td>
              <td>${data.amount/100}</td>
              <td>{data.refunded ? 'Refunded' : data.status}</td>
              <td>{!data.refunded ? <button onClick={(e) => {this.handleRefund(data.id)}}>Refund</button> : ''}</td>
            </tr>
            )
          })}
          </tbody>
        </table>
      </div>
      );
    }

    return (
      <LoginHelper 
        app={this.app} 
        message={loginHelperMessage}
      />
    )
  }
}
 
export default StripeTransactions;