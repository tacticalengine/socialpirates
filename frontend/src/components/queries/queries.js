
import gql from 'graphql-tag';

export const GET_USER_INFO = gql`
  query getUser($userId: Int!){
  users_by_pk(id: $userId){
    id
    user
    email
    role
    userProfile {
      profileID
      displayName
      role
    }
  }
}`;


export const GET_ALL_USERS = gql`

query getallusers($numb: Int, $order: [users_order_by!], $where: users_bool_exp) {
  users(limit: 10, offset: $numb, order_by: $order, where: $where) {
    id
    user
    email
    phone
    isVerified
    role
    twofa
    stripe_cust_ID
    createdAt
    userProfile {
      stripe_plan_ID
      stripe_sub_ID
      role
    }
  }
}
`;

export const GET_POSTS = gql`

query getPosts($postIDs: posting_bool_exp, $limit: Int) {
  posting(where: $postIDs, limit: $limit, order_by: {createdAt: desc}) {
    id
    content
    createdAt
    updatedAt
    belongsTo
  }
}
`;