import React, { Component } from 'react';
import {Button, Form, FeedDate} from 'semantic-ui-react';

import { cookieParser } from '../helpers/cookies';
import Settings from './Settings.jsx';

class PhoneVerification extends Component {
  constructor(props) {
    super(props);

    this.props.isLoggedIn ? this.isLoggedIn = this.props.isLoggedIn : this.isLoggedIn = false;
    
    this.app = this.props.app;

    this.state = {
      showSettings: false,
      verificationStart: false,
      vcode: '',
      successMessage: '',
      errorMessage: ''
    };
    
    this.handleSettings = this.handleSettings.bind(this);
    this.getPhoneInfo = this.getPhoneInfo.bind(this);
    this.handlePhoneChange = this.handlePhoneChange.bind(this);
    this.submitPhoneInfo = this.submitPhoneInfo.bind(this);
    this.showCodeSubmissionForm = this.showCodeSubmissionForm.bind(this);
    this.handleVCodeChange = this.handleVCodeChange.bind(this);
    this.sendVCode = this.sendVCode.bind(this);
  }
  
  getPhoneInfo() {
    this.userService.get(this.userId)
      .then(userInfo => {
        if(userInfo.phone) {
          this.setState({
            phone: userInfo.phone
          });
        }
      });
  }

  submitPhoneInfo() {
    const isValidPhone = phone.checkValidity();
    if (isValidPhone) {
      this.userService.patch(this.userId, {
        phone: this.state.phone
      }).then(() => {
        this.app.service('twofactorauth')
          .create({
            'userId': this.userId,  
            'phone': this.state.phone
          })
          .then(() => {
            this.showCodeSubmissionForm();
          })
          .catch(err => {
            console.log(err);
          });
      });
    }
  }

  handlePhoneChange() {
    let phoneInput = document.getElementById('phone');
    this.setState({
      userId: this.userId,
      phone: phoneInput.value
    });
  }

  handleSettings() {
    this.setState({
      showSettings: true,
      successMessage: '',
      errorMessage: ''
    });
  }

  showCodeSubmissionForm() {
    this.setState({
      verificationStart: true
    });
  }

  handleVCodeChange() {
    let vcodeInput = document.getElementById('vcode');
    this.setState({
      vcode: vcode.value
    });
  }

  sendVCode() {
    this.app.service('twofactorauth')
      .patch(this.userId, {
        'vcode': this.state.vcode
      })
      .then(response => {
        if(response === 'success') {
          this.setState({
            successMessage: 'Two Factor Authentication enabled!',
            errorMessage: '',
            verificationStart: false
          });
        } else {
          this.setState({
            errorMessage: response,
            successMessage: ''
          });
        }
      });
  }

  componentWillMount() {
    // get the userService from the Settings Component
    if(this.props.userService) {
      this.userService = this.props.userService;

      cookieParser('userId', this.app)
        .then(response => {
          this.userId = response;
    
          this.getPhoneInfo();
        })

      if(this.props.token) {
        this.userId = this.props.userId;
    
        this.getPhoneInfo();
      }
    }    
  }

  render() { 
    let verificationStart = this.state.verificationStart;
    let showSettings = this.state.showSettings;
    let errorMessage = this.state.errorMessage;
    let successMessage = this.state.successMessage;
    return (
      <div>
        {showSettings ? (
          <Settings 
            userService={this.userService} 
            isLoggedIn={this.isLoggedIn}
            app={this.app}
          />  
        ): (
          verificationStart ? (
            <div className="container">
              <h1 className="black-heading-center">Verification Code Sent!</h1>
              <Form>
                <input type="number" name="vcode" id="vcode" defaultValue='' onChange={this.handleVCodeChange} />
                <Button onClick={this.sendVCode}>Send</Button>
              </Form>
            </div>
          ) : (
            <div className="container">
              <h1 className="black-heading-center">Thank you for opting for 2 Factor Authentication</h1>
              <h3>This will make your account a lot safer</h3>

              <p>Please verify your phone number</p>
              
              <div className="form-container">
                <Form> 
                  <input type="tel" title="Please enter your 10 digit phone number. ex: 5556667777" pattern="(^(?![1]))(\d{10})" maxLength="10" name="phone" id="phone" defaultValue={this.state.phone} onChange={this.handlePhoneChange} />
                  <Button className="clickable" onClick={this.submitPhoneInfo}>Submit</Button>
                </Form>
              </div>
              <button className="clickable" onClick={this.handleSettings}>Go back to settings</button>
            </div>
          )
        )}

        {successMessage && 
          // form success messages are displayed here only when there is a success message.
          <div className="success-message">
            {successMessage}
          </div>
        }

        {errorMessage && 
          // form error messages are displayed here only when there is an error message.
          <div className="error-message">
            {errorMessage}
          </div>
        }

      </div>
    );
  }
}
 
export default PhoneVerification;