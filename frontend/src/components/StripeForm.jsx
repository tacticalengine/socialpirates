import React from 'react'
import StripeCheckout from 'react-stripe-checkout';
require('dotenv').config();

import config from './../json/config';

const feathers = require('@feathersjs/feathers');
const rest = require('@feathersjs/rest-client');
const auth = require('@feathersjs/authentication-client');
const app = feathers();
import { cookieParser } from '../helpers/cookies';
var moment = require('moment');

// Connect to a different URL
const restClient = rest(config.clientURL);

// Configure an AJAX library (see below) with that client 
app.configure(restClient.fetch(window.fetch))
  .configure(auth(config.auth));

import RegisterHelper from './RegisterHelper.jsx';

export default class StripeForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      registered: false,
      isloggedIn: false,
      purchased: false,
      planEnds: false,
      custID: false
    };

    this.app = app;
  }

  componentWillMount() {
    let theemail;
    let custID;
    let userID;
    cookieParser('userId', this.app)
      .then(response => {
        if (response) {
          userID = response;

          //get the email address from user 
          const userService = this.app.service('users');
          userService.find({
            query: {
              id: {
                '$eq': response
              }
            }
          }).then((userResponse) => {
            for (let x of userResponse.data) {
              theemail = x.email;
              custID = x.stripe_cust_ID;
            }
            this.setState({
              isLoading: false,
              isLoggedIn: true,
              email: theemail,
              userId: userID,
              custID
            })
          })
        }
      }).then(_ => {
        if (userID) {
          this.app.service('users')
            .get(userID)
            .then(response => {
              if (response) {
                const customerId = response.stripe_cust_ID ? response.stripe_cust_ID : null;
                if (customerId) {
                  const profileService = this.app.service('profile');
                  profileService.find({
                    query: {
                      belongsTo: {
                        '$eq': userID
                      }
                    }
                  }).then((response) => {
                    for (let x of response.data) {
                      if (x.role && x.role !== 'user') {
                        this.state.displayName = (x.displayName) ? x.displayName : null;
                        let planend = x.plan_ends;
                        const d = moment.utc(planend * 1000).local().format("MMM DD, YYYY")

                        this.setState({
                          purchased: true,
                          planEnds: d
                        })
                      }
                    }
                  })
                }
              }
            })

        }
      })
  }

  onToken = (token) => {
    if (! this.state.custID) {
      app.service('handler')
        .create({
          token,
          plan: 'plan_EuEe8hId3sHYSW',
          userID: this.state.userID
        })
        .then(response => {
          if (response) {
            this.setState({
              customerData: response,
              registered: true
            })
          };
          if (this.state.isLoggedIn) {
            const cardInfo = response.cardInfo;
            const checkoutService = this.app.service('checkout');
            const profileService = this.app.service('profile');
            const userService = this.app.service('users');
            const custData = response.createData;
            const subscriptionData = response.subscriptionData;
            const userAgent = navigator.userAgent;
            //create checkout
            checkoutService.create({
              'belongsTo': this.state.userId,
              'stripe_customer_ID': custData.id,
              'name': custData.name,
              'email': custData.email,
              'description': custData.description,
              'invoice_prefix': custData.invoice_prefix,
              'discount': custData.discount,
              'default_source': custData.default_source,
              'current_period_start': subscriptionData.current_period_start,
              'current_period_end': subscriptionData.current_period_end,
              'cancel_at_period_end': subscriptionData.cancel_at_period_end,
              'plan_details': subscriptionData.plan,
              'active': subscriptionData.plan.active,
              'paid': subscriptionData.plan.amount,
              'purchased': subscriptionData.plan.product,
              'stripe_plan_ID': subscriptionData.plan.id,
              'stripe_sub_ID': subscriptionData.id,
              'card': cardInfo,
              'userAgent': userAgent

            }).catch((err) => console.log(err));
            userService.patch(this.state.userId, {
              'stripe_cust_ID': custData.id
            })
            //update profile's turn 
            profileService.find({
              query: {
                belongsTo: {
                  '$eq': this.state.userId
                }
              }
            }).then((response) => {
              let profileID;
              if (response.data.length > 0) {
                for (let x of response.data) {
                  profileID = x.profileID;
                }
                profileService.patch(profileID, {
                  'belongsTo': this.state.userId,
                  'stripe_plan_ID': subscriptionData.plan.id,
                  'stripe_sub_ID': subscriptionData.id,
                  'plan_starts': subscriptionData.current_period_start,
                  'plan_ends': subscriptionData.current_period_end,
                  'plan_status': subscriptionData.plan.active,
                  'role': 'Gold'
                }).then((profileResponse) => {
                  let planend = profileResponse.plan_ends;
                  const d = moment.utc(planend * 1000).local().format("MMM DD, YYYY")
                  this.setState({
                    displayName: profileResponse.displayName,
                    planEnds: d,
                    purchased: true

                  })
                })
              }
            }).catch((err) => console.log(err))
          }
        })
    } else {
      console.log(' you are logged in with a customer ID')
    }

  }

  render() {
    const { customerData, registered, isLoggedIn, purchased, email } = this.state;
    const app = this.app;


    if (isLoggedIn && !purchased) {
      return (<div>
        cool you're logged in! Gimme your money!!
<br />

        <StripeCheckout
          token={this.onToken}
          stripeKey={process.env.STRIPE_PUB_KEY}
          panelLabel="Give Money"
          name="socialpirates"
          email={email}
          image="https://media.glamour.com/photos/5cec17dd583d36831269eb26/16:9/w_1280%2Cc_limit/emilia-clarke-game-of-thrones.jpg"
        />
      </div>)
    }

    if (purchased) {
      return (<div>Thank you for being a premium member{(this.state.displayName) ? ', ' + this.state.displayName : null}!
        <p>{(this.state.planEnds) ? 'Your plan ends on ' + this.state.planEnds : null} </p>
        Head over to your new <a href="/dashboard">dashboard</a> with endless features.
      </div>)
    }

    if (registered && !isLoggedIn) {
      return (
        <RegisterHelper
          customerData={customerData}
          app={app}
        />
      )
    }

    if (!registered && !isLoggedIn) {
      return (<div>
        Register with Premium <br />
        <StripeCheckout
          token={this.onToken}
          stripeKey={process.env.STRIPE_PUB_KEY}
          panelLabel="Give Money"
          name="socialpirates"
          image="https://media.glamour.com/photos/5cec17dd583d36831269eb26/16:9/w_1280%2Cc_limit/emilia-clarke-game-of-thrones.jpg"
        />
      </div>
      )
    }
    return ('')
  }
}