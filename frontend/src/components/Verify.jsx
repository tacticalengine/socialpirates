import React, { Component } from 'react';
import LoginHelper from './LoginHelper.jsx';
import {backendURL} from '../helpers/helpers';

class Verify extends Component {
  constructor(props) {
    super(props);

    this.action = this.props.match.params.action;
    this.value = this.props.match.params.value;
    this.verified = false;

    this.state = { 
      redirect: false,
      message: {
        content: '',
        success: false
      }
    }

    this.verifyBasedOnAction = this.verifyBasedOnAction.bind(this);
    this.verifyBasedOnAction(); 
  }

  verifyBasedOnAction() {
    if(this.action === 'verify') {
      var obj = {
        action: 'verifySignupLong',
        value: this.value
      }

      fetch(`${backendURL('be')}/authmanagement`, {
        method: 'POST',
        headers: {
          'Content-Type': 'Application/json'
        },
        body: JSON.stringify(obj)
      })
      .catch(err => {
        document.querySelector(".info").innerHTML = err;
      })
      .then(response => {
        if(response) {
          if(response.statusText === 'Created') {
            this.setState({
              message: {
                content: 'Verification successful please login',
                success: true
              },
              redirect: true
            })
          } else if (response.statusText === 'Bad Request') {
            this.setState({
              message: {
                content: 'Verification failed, try again.',
                success: false
              },
              redirect: true
            })
          }
        }
      })
    }
  }

  render() {
    const { redirect, message } = this.state;

    if(redirect) {
      return ( 
        <LoginHelper message={message} />
      )
    }

    return (
      <div> 
        <div>
          <h2>Verifying...</h2>
          <div className="info"></div>
        </div>
      </div>
    );
  }
}
 
export default Verify;