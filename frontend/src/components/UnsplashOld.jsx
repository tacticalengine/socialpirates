import React, { Component } from 'react';
import Modal from 'react-modal';
import { FormGroup, Label } from 'semantic-ui-react';
import './style/unsplash.styl';

import LoginHelper from './LoginHelper.jsx';

Modal.setAppElement('#react-root');

const initialState = {
  toptext: "", // Top caption of the meme
  bottomtext: "", // Bottom caption of the meme
  isTopDragging: false, // Checking whether top text is repositioned
  isBottomDragging: false,  // Checking whether bottom text is repositioned
  // X and Y coordinates of the top caption
  topY: "10%",  
  topX: "50%",
  // X and Y coordinates of the bottom caption
  bottomX: "50%",
  bottomY: "90%",
  fontcolor: '#fff'
}

class Unsplash extends Component {
  constructor(props) {
    super(props);

    this.app = this.props.app;
    this.isLoggedIn = false;
    this.userId;

    this.state = {
      isLoading: true,
      searchQuery: '',
      showModal: false,
      customWidth: "1200",
      customHeight: "630",
      currentImagebase64: null,

      ...initialState 
    }

    this.checkAuth = this.checkAuth.bind(this);
    this.verifyJWT = this.verifyJWT.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getImages = this.getImages.bind(this);
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.changeText = this.changeText.bind(this);
    this.getStateObj = this.getStateObj.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.getBase64Image = this.getBase64Image.bind(this);

    this.token = this.props.token ? this.props.token : '';

    this.checkAuth(this.token);
  }

  async getBase64Image(chosenImage) {
    let promise = new Promise((resolve, reject) => {
      let dataURL;

      const img = new Image,
      canvas = document.createElement("canvas"),
      ctx = canvas.getContext("2d");
  
      img.crossOrigin = "Anonymous";
  
      img.onload = function() {      
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0);
        // https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/toDataURL
        dataURL = canvas.toDataURL("image/png");
  
        resolve(dataURL);
      }
  
      img.src = chosenImage;
    })
    
    let result = await promise;

    return result;
  }

  openImage(id) {
    this.setState({
      currentImagebase64: null
    })

    this.app.service('unsplash')
      .get(id, {query: {
        width: this.state.customWidth,
        height: this.state.customHeight
      }})
      .then(response => {
        this.chosenImage = response.urls.custom;

        this.getBase64Image(this.chosenImage)
          .then(result => {
            this.setState({ 
              currentImagebase64: result
            })
          });
        this.handleOpenModal();
      })
  }

  // check authentication before displaying form
  async checkAuth(token = '') {
    if(!token) {
      await this.app.authenticate().then((response) => {
        if(response.accessToken.length > 0) {
          // set login status to true if there is a token
          this.verifyJWT(response.accessToken);
        }   
      }).catch(() => {
        this.setState({
          isLoading: false
        });
      });
    } else {
      await this.verifyJWT(token);
    }
  }

  verifyJWT(token) {
    this.app.passport
      .verifyJWT(token)
      .then(response => {
        this.userId = response.userId;
      })

    this.isLoggedIn = true;

    // set loading state to false
    this.setState({
      isLoading: false
    });
  }

  handleOpenModal () {
    this.setState({ showModal: true });
  }
  
  handleCloseModal () {
    this.setState({ showModal: false });
  }

  handleChange(e) {
    e.preventDefault();
    this.setState({[e.target.name]: e.target.value})
  }

  getImages(e) {
    e.preventDefault();
    
    this.setState({
      isLoading: true
    })

    this.app.service('unsplash')
      .find({
        query: {
          searchQuery: this.state.searchQuery,
          limit: 12
        }
      })
      .then(response => {
        if(response) {
          this.imageData = response
          this.setState({
            isLoading: false
          })
        } else {
          console.log('error contacting Unsplash server');
        }
      })
      .catch(err => {
        console.log('error contacting Unsplash server');
      })
  }
    
  changeText(e) {
    this.setState({
      [e.currentTarget.name]: e.currentTarget.value
    });
  }

  getStateObj(e, type) {
    let rect = this.imageRef.getBoundingClientRect();
    // This getBoundingClientRect() returns height, width and positions of the element
    // In our case, we get the image's positions in the DOM since we position the text on the image.
    const xOffset = e.clientX - rect.left;
    const yOffset = e.clientY - rect.top;
    // This calculation yields us the current x and y positions of the element/cursor.
    let stateObj = {};
    // This is common function for top and bottom captions.
    if (type === "bottom") {
      stateObj = {
        isBottomDragging: true,
        isTopDragging: false,
        bottomX: `${xOffset}px`,
        bottomY: `${yOffset}px`
      }
    } else if (type === "top") {
      stateObj = {
        isTopDragging: true,
        isBottomDragging: false,
        topX: `${xOffset}px`,
        topY: `${yOffset}px`
      }
    }
    return stateObj;
  }
  
  handleMouseDown(e, type) {
    const stateObj = this.getStateObj(e, type);
    // Finding current co-ordinates of the dragged <text />
    document.addEventListener('mousemove', (event) => this.handleMouseMove(event, type));
    // Start tracking the mouse movement.
    this.setState({
      ...stateObj
    })
  }
  
  handleMouseMove(e, type) {
    if (this.state.isTopDragging || this.state.isBottomDragging) {
      // Only if dragging is active in the state, track mouse movements.
      let stateObj = {};
      if (type === "bottom" && this.state.isBottomDragging) {
        stateObj = this.getStateObj(e, type); // Getting the co-ordinates for bottom caption
      } else if (type === "top" && this.state.isTopDragging){
        stateObj = this.getStateObj(e, type); // Getting the co-ordinates for top caption
      }
      this.setState({
        ...stateObj
      });
    }
  };
  
  handleMouseUp() {
    // If mouse is released, remove the event listener and terminate drag actions.
    document.removeEventListener('mousemove', this.handleMouseMove);
    this.setState({
      isTopDragging: false,
      isBottomDragging: false
    });
  }

  convertSvgToImage() {
    const svg = this.svgRef;
    let svgData = new XMLSerializer().serializeToString(svg);
    const canvas = document.createElement("canvas");
    canvas.setAttribute("id", "canvas");
    const svgSize = svg.getBoundingClientRect();
    canvas.width = svgSize.width;
    canvas.height = svgSize.height;
    const img = document.createElement("img");
    img.setAttribute("src", "data:image/svg+xml;base64," + btoa(unescape(encodeURIComponent(svgData))));
    img.onload = function() {
      canvas.getContext("2d").drawImage(img, 0, 0);
      const canvasdata = canvas.toDataURL("image/png");
      const a = document.createElement("a");
      a.download = "meme.png";
      a.href = canvasdata;
      document.body.appendChild(a);
      a.click();
    };
  }

  render() { 
    const isLoggedIn = this.isLoggedIn;
    let isLoading = this.state.isLoading;

    const textStyling = {
      fontSize: '32px',
      fill: this.state.fontcolor,
      cursor: 'pointer'
    }

    if(isLoading) {
      return (
        <div>Loading...</div>
      )
    }

    if(isLoggedIn) {
      return (
        <div className="container">
          <h2>Search for Images</h2>
          <form name="unsplashImageSearch" onSubmit={this.getImages}>
            <input type="text" name="searchQuery" placeholder="Enter Search Term" onChange={this.handleChange}></input>
            <button type="submit">Search</button>
          </form>

          <div className="images-container">
            <Modal 
              isOpen={this.state.showModal}
              contentLabel="Image Editor"
              onRequestClose={this.handleCloseModal}
              shouldCloseOnOverlayClick={true}
            >
              <svg
                ref={el => { this.svgRef = el }}
                height={this.state.customHeight}
                width={this.state.customWidth}
                xmlns="http://www.w3.org/2000/svg"
                xmlnshlink="http://www.w3.org/1999/xlink">
                <image
                  ref={el => { this.imageRef = el }}
                  height={this.state.customHeight}
                  width={this.state.customWidth}
                  xlinkHref={this.state.currentImagebase64}
                />
                <text
                  style={{ ...textStyling, zIndex: this.state.isTopDragging ? 4 : 1 }}
                  x={this.state.topX}
                  y={this.state.topY}
                  dominantBaseline="middle"
                  textAnchor="middle"
                  onMouseDown={event => this.handleMouseDown(event, 'top')}
                  onMouseUp={event => this.handleMouseUp(event, 'top')}
                >
                    {this.state.toptext}
                </text>
                <text
                  style={textStyling}
                  dominantBaseline="middle"
                  textAnchor="middle"
                  x={this.state.bottomX}
                  y={this.state.bottomY}
                  onMouseDown={event => this.handleMouseDown(event, 'bottom')}
                  onMouseUp={event => this.handleMouseUp(event, 'bottom')}
                >
                    {this.state.bottomtext}
                </text>
              </svg>
              <div className="imageEditorForm">
                <FormGroup>
                  <Label htmlFor="toptext">Top Text</Label>
                  <input type="text" name="toptext" id="toptext" placeholder="Add text to the top" onChange={this.changeText} />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="bottomtext">Bottom Text</Label>
                  <input type="text" name="bottomtext" id="bottomtext" placeholder="Add text to the bottom" onChange={this.changeText} />
                </FormGroup>

                <FormGroup>
                  <Label htmlFor="fontcolor">Font Color</Label>
                  <input type="text" name="fontcolor" id="fontcolor" placeholder="Color code" onChange={this.changeText} />
                </FormGroup>
                <button onClick={() => this.convertSvgToImage()}>Download Meme!</button>
              </div>
            </Modal>
            {this.imageData ? (
              this.imageData === 'empty' ? (
                <div>No images were found.</div>
              ) : (
                this.imageData.map((image, index) => {
                  return (
                    <div className="image-handler" key={image.id}>
                      <img 
                        src={image.urls.thumb} 
                        alt={image.alt_description}
                        onClick={() => { this.openImage(image.id)}} 
                      />
                      <p>Photo by {image.user.username} on Unsplash</p>
                    </div>
                  );
                })


              )) : (
              <div>
                Search using the box above.
              </div>
            )}
          </div>
        </div>
      )
    }    

    return ( 
      <LoginHelper app={this.app} />
     );
  }
}
 
export default Unsplash;