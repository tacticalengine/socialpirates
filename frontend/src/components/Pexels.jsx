import React, { Component } from 'react';

import LoginHelper from './LoginHelper.jsx';

class Pexels extends Component {
  constructor(props) {
    super(props);

    this.app = this.props.app;
    this.isLoggedIn = false;
    this.userId;

    this.state = {
      isLoading: true
    }

    this.checkAuth = this.checkAuth.bind(this);
    this.verifyJWT = this.verifyJWT.bind(this);

    this.token = this.props.token ? this.props.token : '';

    this.checkAuth(this.token);
  }

  // check authentication before displaying form
  async checkAuth(token = '') {
    if(!token) {
      await this.app.authenticate().then((response) => {
        if(response.accessToken.length > 0) {
          // set login status to true if there is a token
          this.verifyJWT(response.accessToken);
        }   
      }).catch(() => {
        this.setState({
          isLoading: false
        });
      });
    } else {
      await this.verifyJWT(token);
    }
  }

  verifyJWT(token) {
    this.app.passport
      .verifyJWT(token)
      .then(response => {
        this.userId = response.userId;
      })

    this.isLoggedIn = true;

    // set loading state to false
    this.setState({
      isLoading: false
    });
  }

  componentWillMount() {
    this.app.service('pexels')
      .find({
        query: {
          searchQuery: 'coolness',
          limit: 10,
          page: 1
        }
      })
      .then(response => {
        console.log(response);
        if(response.query) {
          console.log(response.query);
        } else {
          console.log('error contacting pexels server');
        }
      })
      .catch(err => {
        console.log(err);
        console.log('error contacting pexels server');
      })
  }

  render() { 
    const isLoggedIn = this.isLoggedIn;
    let isLoading = this.state.isLoading;

    if(isLoading) {
      return (
        <div>Loading...</div>
      )
    }

    if(isLoggedIn) {
      return (
        <div></div>
      )
    }    

    return ( 
      <LoginHelper app={this.app} />
     );
  }
}
 
export default Pexels;