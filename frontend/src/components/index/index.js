//This will be inline in the head
require('./style/head.css');

//including all stylus files

const context = require.context('./', true, /\.(styl)$/);
let files={};
context.keys().forEach((filename)=>{
  files[filename] = context(filename);
});

//The react file
require('./react.js');

//The main index file where react & css will be inserted
require('./index.hbs');

// a second way to put in data to handlebars templates
// var index = require('./index.hbs'); 

// document.addEventListener("DOMContentLoaded", function() {
// 	var div = document.querySelector('.profile');
// 	div.innerHTML = index({
//     title: "Hello World from Handlebars",
//     loginText: "For the login/logout feature"
// 	});
// });

// eslint-disable-next-line no-console
console.log(`Environment: ${process.env.NODE_ENV}`);
