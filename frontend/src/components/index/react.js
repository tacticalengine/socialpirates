import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider, Query } from 'react-apollo';
import  ApolloClient from 'apollo-boost';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';

import config from './../../json/config';

const feathers = require('@feathersjs/feathers');
const rest = require('@feathersjs/rest-client');
const auth = require('@feathersjs/authentication-client');
const app = feathers();

// Connect to a different URL
const restClient = rest(config.clientURL);

// Configure an AJAX library (see below) with that client 
app.configure(restClient.fetch(window.fetch))
  .configure(auth(config.auth));

import AdminPanel from './../AdminPanel.jsx';

var path = require('path');

import LoginHelper from './../LoginHelper.jsx';
import Verify from './../Verify.jsx';
import Dashboard from './../Dashboard.jsx';
import StripeForm from './../StripeForm.jsx';
import ResetPasswd from './../ResetPasswd.jsx';
import ChangePassword from '../ChangePassword.jsx';
import StripeTransactionsAdmin from '../StripeTransactionsAdmin.jsx';
import Pexels from '../Pexels.jsx';
import Unsplash from '../Unsplash.jsx';

const cache = new InMemoryCache({
  cacheRedirects: {
    Query: {
      users: (_, {id }, { getCacheKey }) => getCacheKey({ id, __typename: 'User'})
    }
  },
});

const client = new ApolloClient({
  uri: process.env.HASURA_URL,
  headers: {
    'x-hasura-admin-secret': process.env.HASURA_KEY,
  },
  cache,
  credentials: 'include'
});

const App = () => (
  <ApolloProvider client={client}>
    <Router>
      <Route exact path="/" component={LoginHelper} />
      <Route path="/system/:action?/:value?" component={Verify} />
      <Route 
        path="/pirates/:page?" 
        render={props => <AdminPanel {...props} app={app} />} 
      />
      <Route path="/checkout" component={StripeForm} />
      <Route 
        path="/dashboard/:page?" 
        render={props => <Dashboard {...props} app={app} />} 
      />
      <Route 
        path="/reset/:value?" 
        render={props => <ResetPasswd {...props} app={app} />}
      />
      <Route 
        path="/changepwd/" 
        render={props => <ChangePassword {...props} app={app} />}
      />
      <Route 
        path="/pirates-billing/:id?" 
        render={props => <StripeTransactionsAdmin {...props} app={app} />}
      />
      <Route 
        path="/pexels/"
        render={props => <Pexels {...props} app={app} />}
      />
      <Route 
        path="/unsplash/"
        render={props => <Unsplash {...props} app={app} />}
      />
    </Router>
  </ApolloProvider>
);

ReactDOM.render(
  <App />,
  document.getElementById('react-root')
);

if (module.hot) {
  module.hot.accept();
}