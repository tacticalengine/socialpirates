import React, { Component } from 'react';
import './style/adpanel.styl';
import { GET_ALL_USERS, GET_USER_INFO } from './queries/queries';
import { DELETE_USER } from './mutations/mutations';
import { Query, Mutation } from 'react-apollo';
import { Form, Button, Icon } from 'semantic-ui-react';
import { cookieParser, cookieEncrypt } from '../helpers/cookies';
import LoginHelper from './LoginHelper.jsx';
import Dashboard from './Dashboard.jsx';
import { adopt } from 'react-adopt';
require('dotenv').config();

class AdminPanel extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      number: 0,
      userId: this.props.userId,
      sortBy: 'id',
      sortIt: 'desc',
      isLoggedin: false,
      isLoading: true,
      where: {}
    };

    this.app = this.props.app;
    this.uCool = false;

    this.prev = React.createRef();
    this.next = React.createRef();
    this.users = React.createRef();
    this.increment = this.increment.bind(this);
    this.decrement = this.decrement.bind(this);
    this.sortThis = this.sortThis.bind(this);
    this.doThis = this.doThis.bind(this);
    this.search = this.search.bind(this);
    this.clear = this.clear.bind(this);
    this.handleViewAsUser = this.handleViewAsUser.bind(this);

    this.setIt();
  }

  clear() {
    window.location = '/pirates/';
  }

  search(e) {
    e.preventDefault();
    const value = document.searchForm.search.value;
    //checking value if it contains only numbers or contains @ symbol
    if (value.match(/^[0-9]*$/)) {
      this.setState({
        number: 0,
        where: {
          'id': {
            '_eq': value
          }
        }
      });
    } else if (value.match(/@/)) {
      this.setState({
        number: 0,
        where: {
          'email': {
            '_eq': value
          }
        }
      });
    } else {
      this.setState({
        number: 0,
        where: {
          'user': {
            '_eq': value
          }
        }
      });
    }
  }

  doThis(one) {
    const location = window.location.pathname.split('/');
    const second = location[2].split('-');
    this.state.sortBy = one;
    if (second[1] == 'desc') {
      this.state.sortIt = 'asc';
    } else {
      this.state.sortIt = 'desc';
    }

    window.location = `/pirates/${this.state.sortBy}-${this.state.sortIt}/${this.state.number}`;
  }

  sortThis(e) {
    const item = e.target.innerHTML;
    let newItem;
    switch (item) {
      case 'ID':
        newItem = 'id';
        this.doThis(newItem);
        break;
      case 'Username':
        newItem = 'user';
        this.doThis(newItem);
        break;
      case 'Email':
        newItem = 'email';
        this.doThis(newItem);
        break;
      case 'Verified':
        newItem = 'isVerified';
        this.doThis(newItem);
        break;
      case '2FA':
        newItem = 'twofa';
        this.doThis(newItem);
        break;
      case 'Phone':
        newItem = 'phone';
        this.doThis(newItem);
        break;
      case 'Stripe Customer ID':
        newItem = 'stripe_cust_ID';
        this.doThis(newItem);
        break;
      case 'Role':
        newItem = 'role';
        this.doThis(newItem);
        break;
      case 'Created At':
        newItem = 'createdAt';
        this.doThis(newItem);
        break;
      default:
        newItem = 'id';
        this.doThis(newItem);
    }
  }

  setIt() {
    this._isMounted = true;
    if (this._isMounted) {
      const location = window.location.pathname;
      const split = location.split('/');
      const number = (split[3]) ? parseInt(split[3]) : 0;
      const sort = (split[2]) ? split[2].split('-') : ['id', 'desc'];
      this.state.sortBy = sort[0] ? sort[0] : this.state.sortBy;
      this.state.sortIt = sort[1] ? sort[1] : this.state.sortIt;
      this.state.number = number ? number : this.state.number;
    }
  }

  increment() {
    const location = this.props.location.pathname.split('/');
    const second = location[3];
    const addit = second ? parseInt(second) + 10 : 10;
    window.location = `/pirates/${this.state.sortBy}-${this.state.sortIt}/${addit}`;
  }

  decrement() {
    const location = this.props.location.pathname.split('/');
    const second = location[3];
    const subit = second ? parseInt(second) - 10 : 0;
    window.location = `/pirates/${this.state.sortBy}-${this.state.sortIt}/${subit}`;
  }

  async handleLoginAs(userId) {
    await fetch(process.env.NODE_ENV === 'development' ? `${process.env.BACKENDURL_LOCAL}/authentication/` : `${process.env.BACKENDURL_PROD}/authentication/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'Application/json',
        'userId': userId,
        'x-api-key': process.env.PIRATE_APIKEY
      }
    }).then(response => {
      response.json()
        .then(data => {
          const token = data.accessToken;

          this.handleViewAsUser(token);
        })
    })
      .catch(err => {
        console.log(err);
      })
  }

  handleViewAsUser(token) {
    // generate a cookie in the users browser
    cookieEncrypt('token', token, this.app)
      .then(() => {
        window.location = '/dashboard/';
      })
  }

  componentDidMount() {
    if (this._isMounted) {
      document.addEventListener('DOMContentLoaded', () => {
        setTimeout(() => {
          const location = this.props.location.pathname.split('/');
          const second = (location[3]) ? parseInt(location[3]) : 0;
          const users = (this.users.current) ? this.users.current.children.length : 10;
          if (location[3]) {
            if (second <= 0) {
              const prev = this.prev.current;
              prev.remove();
            }
          }
          if (users <= 9) {
            const next = this.next.current;
            next.remove();
          }
        }, 200);
      });
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentWillMount() {
    cookieParser('userId', this.app)
      .then(response => {
        if (response) {
          this.setState({
            userId: response,
            isLoading: false,
            isLoggedIn: true
          })
        }

      })
  }

  render() {
    const { isLoggedIn, isLoading } = this.state;
    let uCool = this.uCool;
    let users;
    let items;
    const { userId } = this.state;
    const Composed = (this._isMounted) ? adopt({
      userInfo: ({ render }) => (
        <Query query={GET_USER_INFO} variables={{ userId: userId }}>
          {render}
        </Query>),
      allUsers: ({ render }) => (
        <Query query={GET_ALL_USERS} variables={{ numb: this.state.number, order: { [this.state.sortBy]: this.state.sortIt }, where: this.state.where }}>
          {render}
        </Query>),
      deleteUser: ({ render }) => (
        <Mutation mutation={DELETE_USER}>
          {(mutation, data) => render({ mutation, data })}
        </Mutation>)
    }) : '';

    if (isLoading) {
      return (
        <div>Loading...</div>
      )
    }

    return (
      <div className="adminContainer">
        {isLoggedIn && this._isMounted ? (
          <div>
            <Composed>
              {({ userInfo, allUsers, deleteUser: { mutation, data } }) => {
                if (userInfo.data) {
                  for (let individual in userInfo.data) {
                    let itsMe = userInfo.data[individual];
                    let role = itsMe.role;
                    if (role === process.env.ROLE) {
                      uCool = true;
                    }
                  }
                }
                if (allUsers.data) {
                  for (let individual in allUsers.data) {
                    const handleDelete = (e) => {
                      e.preventDefault();
                      const user = e.target.dataset.username;
                      const userId = e.target.dataset.userid;
                      const conf = confirm(`Are you sure you want to delete ${user}`);
                      if (conf == true) {
                        mutation({ variables: { userId: userId } }).then(() => {
                          console.log(user + ' was deleted successfully');
                          location.reload();
                        }).catch((err) => console.log(err));
                      }
                    };
                    users = allUsers.data[individual];
                    items = users.map((item, key) =>
                      <li key={item.id}>
                        <button onClick={handleDelete} data-userid={item.id} data-username={item.user}>Delete</button>
                        <a href={`/pirates-billing/${item.id}/`}>Manage Billing</a>
                        <span><b>ID: </b>{item.id}</span>
                        <span><b>Username: </b>{item.user}</span>
                        <span><b>Email: </b>{item.email}</span>
                        <span><b>Verified: </b>{(item.isVerified) ? '✔' : '✗'}</span>
                        <span><b>2FA: </b>{(item.twofa) ? '✔' : '✗'}</span>
                        <span><b>Phone: </b>{(item.phone) ? item.phone : '✗'}</span>
                        <span><b>Role: </b>{(item.role) ? item.role : '✗'}</span>
                        <span><b>Stripe Customer ID: </b>{(item.stripe_cust_ID) ? item.stripe_cust_ID : '✗'}</span>
                        <span><b>Created At: </b>{item.createdAt}</span>
                        <span><b>Stripe Plan ID: </b>{(item.userProfile[0]) ? item.userProfile.map((e) => (e.stripe_plan_ID) ? e.stripe_plan_ID : '✗') : '✗'}</span>
                        <span><b>Stripe SUB ID: </b>{(item.userProfile[0]) ? item.userProfile.map((e) => (e.stripe_sub_ID) ? e.stripe_sub_ID : '✗') : '✗'}</span>
                        <span><b>Plan Role: </b>{(item.userProfile[0]) ? item.userProfile.map((e) => (e.role) ? e.role : '✗') : '✗'}</span>
                        <span><button onClick={() => { this.handleLoginAs(item.id) }}>Login as user</button></span>
                      </li>
                    );
                  }
                }
                return (
                  <div>
                    {uCool && this._isMounted ? (
                      <div>
                        <h1>Welcome to our lair</h1>
                        <h2>For right now we can only view and delete users</h2>
                        Get back to dashboard: <a href="/">here</a>

                        <div>
                          <span className="search">
                            <Form className="searchForm" name="searchForm" onSubmit={this.search}>
                              <label htmlFor="find">Search for a user</label>
                              <input id="find" type="sarch" name="search" placeholder="ID, Email, or Username" />
                              <Button>Search</Button>
                            </Form>
                            <button className="clear" onClick={this.clear}>clear</button>
                          </span>
                          <div>
                            <span>Sort By: </span>
                            <span><button onClick={this.sortThis}>ID</button></span>
                            <span><button onClick={this.sortThis}>Username</button></span>
                            <span><button onClick={this.sortThis}>Email</button></span>
                            <span><button onClick={this.sortThis}>Verified</button></span>
                            <span><button onClick={this.sortThis}>2FA</button></span>
                            <span><button onClick={this.sortThis}>Phone</button></span>
                            <span><button onClick={this.sortThis}>Stripe Customer ID</button></span>
                            <span><button onClick={this.sortThis}>Role</button></span>
                            <span><button onClick={this.sortThis}>Created At</button></span>
                          </div>
                          <div className="prevnext">
                            <button id="prev" ref={this.prev} onClick={this.decrement}>prev</button>
                            <button id="next" ref={this.next} onClick={this.increment}>next</button>
                          </div>
                          <ul ref={this.users} className="userlist" id="users">
                            {items}
                          </ul>
                        </div>
                      </div>) : (<div>
                        You don't belong here!
                      Get back to dashboard: <a href="/">here</a>
                      </div>)}
                  </div>
                );
              }}
            </Composed>

          </div>

        ) : (<LoginHelper id={this.state.userId} />)}

      </div>
    );
  }
}

export default AdminPanel;
