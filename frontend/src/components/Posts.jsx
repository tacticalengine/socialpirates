
import React, { Component } from 'react';
import { Button, Form, TextArea } from 'semantic-ui-react';
import './style/post.styl';

class Posts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userID: this.props.userID,
      lastContent: ''
    }

    this.preview = React.createRef();
    this.count = React.createRef();
    this.error = React.createRef();
    this.fillIt = this.fillIt.bind(this);
    this.postSubmit = this.postSubmit.bind(this);
    this.app = this.props.app;
    this.postService = this.app.service('posting');
  }

  componentDidMount() {
    this.postService
      .find({
        query: {
          $limit: 1,
          $sort: {
            createdAt: -1
          }
        }
      }).then(response => {
        if (response.data) {
          this.setState({
            lastContent: response.data[0].content
          })
        }
      })
  }

  fillIt(e) {
    let previewText = e.target.value;
    let innerText = this.preview.current;
    let count = this.count.current;
    innerText.innerHTML = previewText;
    count.innerHTML = innerText.innerHTML.length + ' characters';
  }

  postSubmit(e) {
    const text = this.preview.current.innerHTML;
    let errorText = this.error.current;
    let preview = this.preview.current;
    let count = this.count.current;
    let postText = document.getElementById('postText');
    let button = document.getElementById('submitButton');
    if (text == this.state.lastContent) {
      errorText.innerHTML = `Sorry! This post is a duplicate of your last post. If this was
      not a mistake, please modify it.`;
    } else if (text == '') {
      errorText.innerHTML ='Sorry! Your post is empty!';
    } else {
      this.postService
        .create({
          'content': text,
          'belongsTo': this.state.userID
        }).then(() => {
          //Clear everything and disable the button
          preview.innerHTML = '';
          count.innerHTML = '';
          postText.value = '';
          button.disabled = true;
          button.className = 'button successAnimation';
          button.innerHTML = 'success';
        }).then(() => {
          //after 2 seconds re enable the submit button
          setTimeout(() => {
            button.disabled = false;
            button.className = 'button enabled';
            button.innerHTML = 'submit';
            this.setState({
              lastContent: text
            });
          }, 2000);
          
        })
        .catch(err => {
          console.log(err);
          errorText.innerHTML = err;
        })
    }

  }

  render() {

    return (
      <div className="postContainer">
        <h2>Create a Post here</h2>
        <Form onSubmit={this.postSubmit}>
          <TextArea id="postText" placeholder="post" onChange={this.fillIt} />
          <Button id="submitButton" className="enabled">Submit</Button>
        </Form>
        <p className="error" ref={this.error}></p>

        <div className="previewContainer">
          Preview
          <div ref={this.preview}></div>
          <p ref={this.count}>0 characters</p>
        </div>

      </div>
    )
  }
}

export default Posts;