import React, { Component } from 'react';
import Modal from 'react-modal';
import 'tui-image-editor/dist/tui-image-editor.css';
import ImageEditor from '@toast-ui/react-image-editor';
import './style/unsplash.styl';

const myTheme = {
  // Theme object to extends default dark theme.
    'common.bi.image': '/images/image-editor.jpg',
    'common.bisize.width': '151px',
    'common.bisize.height': '21px',
    'common.backgroundImage': 'none',
    'common.border': '0px',

    // header
    'header.backgroundImage': 'none',
    'header.backgroundColor': 'transparent',
    'header.border': '0px',

    // load button
    'loadButton.display': 'none',
    // 'loadButton.Text': 'Soemthing',
    // 'loadButton.backgroundColor': '#fff',
    // 'loadButton.border': '1px solid #ddd',
    // 'loadButton.color': '#222',
    // 'loadButton.fontFamily': 'NotoSans, sans-serif',
    // 'loadButton.fontSize': '12px',

    // download button
    'downloadButton.backgroundColor': '#fdba3b',
    'downloadButton.border': '1px solid #fdba3b',
    'downloadButton.color': '#fff',
    'downloadButton.fontFamily': 'NotoSans, sans-serif',
    'downloadButton.fontSize': '12px',

    // main icons
    'menu.normalIcon.path': '/images/svg/icon-b.svg',
    'menu.normalIcon.name': 'icon-b',
    'menu.activeIcon.path': '/images/svg/icon-a.svg',
    'menu.activeIcon.name': 'icon-a',
    'menu.disabledIcon.path': '/images/svg/icon-c.svg',
    'menu.disabledIcon.name': 'icon-c',
    'menu.hoverIcon.path': '/images/svg/icon-d.svg',
    'menu.hoverIcon.name': 'icon-d',
    'menu.iconSize.width': '24px',
    'menu.iconSize.height': '24px',

    // submenu primary color
    'submenu.backgroundColor': '#1e1e1e',
    'submenu.partition.color': '#858585',

    // submenu icons
    'submenu.normalIcon.path': '/images/svg/icon-a.svg',
    'submenu.normalIcon.name': 'icon-a',
    'submenu.activeIcon.path': '/images/svg/icon-c.svg',
    'submenu.activeIcon.name': 'icon-c',
    'submenu.iconSize.width': '32px',
    'submenu.iconSize.height': '32px',

    // submenu labels
    'submenu.normalLabel.color': '#858585',
    'submenu.normalLabel.fontWeight': 'lighter',
    'submenu.activeLabel.color': '#fff',
    'submenu.activeLabel.fontWeight': 'lighter',

    // checkbox style
    'checkbox.border': '1px solid #ccc',
    'checkbox.backgroundColor': '#fff',

    // rango style
    'range.pointer.color': '#fff',
    'range.bar.color': '#666',
    'range.subbar.color': '#d1d1d1',
    'range.value.color': '#fff',
    'range.value.fontWeight': 'lighter',
    'range.value.fontSize': '11px',
    'range.value.border': '1px solid #353535',
    'range.value.backgroundColor': '#151515',
    'range.title.color': '#fff',
    'range.title.fontWeight': 'lighter',

    // colorpicker style
    'colorpicker.button.border': '1px solid #1e1e1e',
    'colorpicker.title.color': '#fff'
};

import LoginHelper from './LoginHelper.jsx';

Modal.setAppElement('#react-root');

class Unsplash extends Component {
  constructor(props) {
    super(props);

    this.app = this.props.app;
    this.isLoggedIn = false;
    this.userId;

    this.state = {
      isLoading: true,
      searchQuery: '',
      showModal: false,
      customWidth: "1200",
      customHeight: "630",
      currentPage: null,
      errorMessage: ''
    }

    this.checkAuth = this.checkAuth.bind(this);
    this.verifyJWT = this.verifyJWT.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getImages = this.getImages.bind(this);
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
   
    this.token = this.props.token ? this.props.token : '';
    this.lastPage = false;

    this.checkAuth(this.token);
  }

  openImage(id) {
    this.app.service('unsplash')
      .get(id, {query: {
        width: this.state.customWidth,
        height: this.state.customHeight
      }})
      .then(response => {
        this.chosenImageCustom = response.urls.custom;
        this.chosenImage = response.urls.regular;
        this.chosenImageTitle = response.alt_description;

        this.handleOpenModal();
      })
  }

  // check authentication before displaying form
  async checkAuth(token = '') {
    if(!token) {
      await this.app.authenticate().then((response) => {
        if(response.accessToken.length > 0) {
          // set login status to true if there is a token
          this.verifyJWT(response.accessToken);
        }
      }).catch(() => {
        this.setState({
          isLoading: false
        });
      });
    } else {
      await this.verifyJWT(token);
    }
  }

  verifyJWT(token) {
    this.app.passport
      .verifyJWT(token)
      .then(response => {
        this.userId = response.userId;
      })

    this.isLoggedIn = true;

    // set loading state to false
    this.setState({
      isLoading: false
    });
  }

  handleOpenModal () {
    this.setState({ showModal: true });
  }
  
  handleCloseModal () {
    this.setState({ showModal: false });
  }

  handleChange(e) {
    e.preventDefault();
    this.setState({[e.target.name]: e.target.value})
  }

  getImages(e) {
    e.preventDefault();

    const input = document.unsplashImageSearch.searchQuery.value;

    if(input === '') {
      this.setState({
        errorMessage: 'Please enter a search term.'
      })

      return;
    } else {
      this.setState({
        isLoading: true,
        errorMessage: ''
      })
    }
 
    this.app.service('unsplash')
      .find({
        query: {
          searchQuery: this.state.searchQuery,
          limit: 12
        }
      })
      .then(response => {
        if(response) {
          if(response.photos.length === 0) {
            this.setState({
              errorMessage: 'No results found',
              isLoading: false
            })

            return;
          }
          this.imageData = response.photos;
          this.pageData = response.numberOfPages;
          
          this.setState({
            isLoading: false,
            currentPage: 1
          })
        } else {
          this.setState({
            errorMessage: 'error contacting Unsplash server',
            isLoading: false
          })
        }
      })
      .catch(() => {
        this.setState({
          errorMessage: 'error contacting Unsplash server',
          isLoading: false
        })
      })
  }    

  handlePageChange(e) {
    e.preventDefault();

    let currentPage;

    if(e.target.parentNode.classList[0] === 'prev-button') {
      currentPage = this.state.currentPage - 1;
    } else {
      currentPage = this.state.currentPage + 1;
    }

    this.app.service('unsplash')
    .find({
      query: {
        searchQuery: this.state.searchQuery,
        limit: 12,
        page: currentPage
      }
    })
    .then(response => {
      if(response) {
        this.imageData = response.photos;

        if(currentPage === parseInt(this.pageData)) {
          this.lastPage = true;
        }
        
        this.setState({
          currentPage
        })
      } else {
        this.setState({
          errorMessage: 'error contacting Unsplash server',
        })
      }
    })
    .catch(() => {
      this.setState({
        errorMessage: 'error contacting Unsplash server',
      })
    })
  }

  render() { 
    const {isLoggedIn, lastPage} = this;
    let {isLoading, currentPage, errorMessage} = this.state;

    if(isLoading) {
      return (
        <div>Loading...</div>
      )
    }

    if(isLoggedIn) {
      return (
        <div className="container">
          <div className="form-container">
            <h2>Search for Images</h2>
            <form name="unsplashImageSearch" onSubmit={this.getImages}>
              <input type="text" name="searchQuery" placeholder="Enter Search Term" onChange={this.handleChange}></input>
              <button type="submit">Search</button>
            </form>
            {errorMessage && 
              <div className="error-message">{errorMessage}</div>
            }
          </div>

          <div>
            <Modal 
              isOpen={this.state.showModal}
              contentLabel="Image Editor"
              onRequestClose={this.handleCloseModal}
              shouldCloseOnOverlayClick={true}
            >
              <ImageEditor 
                includeUI={{
                  loadImage: {
                    path:  this.chosenImage,
                    name: this.chosenImageTitle
                  },
                  theme: myTheme,
                  uiSize: {
                    width: '100%',
                    height: '100%'
                  },
                  menuBarPosition: 'top'
                }}
                usageStatistic={false}
              />
            </Modal>
            {this.imageData ? (
              this.imageData === 'empty' || this.imageData.length === 0 ? (
                <section>No images were found.</section>
              ) : (
                <section>
                  <h3 className="centered">Click the image you want to edit:</h3>
                  <div className="images-container">
                    {this.imageData.map((image, index) => {
                      return (
                        <div className="image-handler" key={image.id}>
                          <img 
                            src={image.urls.thumb} 
                            alt={image.alt_description}
                            onClick={() => { this.openImage(image.id)}} 
                          />
                          <p>Photo by {image.user.username} on Unsplash</p>
                        </div>
                      );
                    })}
                  </div>
                </section>
              )) : (
              <section>
                Search using the box above.
              </section>
            )}
          </div>
          
          {this.pageData ? (
            this.pageData === 0 ? ( 
              ''
            ) : (
              <section className="pagination-container">
                {currentPage > 1 && 
                <div className="prev-button">
                  <button onClick={this.handlePageChange}>Previous</button>
                </div>
                }
                {lastPage ? (
                  ''
                ) : ( 
                  <div className="next-button">
                    <button onClick={this.handlePageChange}>Next</button>
                  </div>
                )}
              </section>
            )) : (
              ''
            )
          }
        </div>
      )
    }    

    return ( 
      <LoginHelper app={this.app} />
     );
  }
}
 
export default Unsplash;