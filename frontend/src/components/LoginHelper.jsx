import React, { Component } from 'react';
import { Button, Form } from 'semantic-ui-react';
import './style/login.styl';

import RegisterHelper from './RegisterHelper.jsx';
import Dashboard from './Dashboard.jsx';
import ResetPasswd from './ResetPasswd.jsx';

import config from './../json/config';
import { cookieEncrypt } from '../helpers/cookies';

const feathers = require('@feathersjs/feathers');
const rest = require('@feathersjs/rest-client');
const auth = require('@feathersjs/authentication-client');
const app = feathers();

// Connect to a different URL
const restClient = rest(config.clientURL);

// Configure an AJAX library (see below) with that client 
app.configure(restClient.fetch(window.fetch))
  .configure(auth(config.auth));

// app.use(passwordProtected(passwordConfig));
class LoginHelper extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: '',
      password: '',
      isLoading: true,
      errorMessage: '',
      successMessage: '',
      register: false,
      resetPasswd: false
    };

    this.handleLogin = this.handleLogin.bind(this);
    this.checkAuth = this.checkAuth.bind(this);
    this.printError = this.printError.bind(this);
    this.switchForm = this.switchForm.bind(this);
    this.handleResetPasswd = this.handleResetPasswd.bind(this);

    this.isLoggedIn = false;
    this.app = app;

    this.checkAuth();
  }

  componentDidMount() {
    this._isMounted = true;
    if (this.props.message) {
      if (this.props.message.content.length > 0 && this.props.message.success) {
        this.setState({
          successMessage: this.props.message.content
        });
      } else if (this.props.message.content.length > 0 && !this.props.message.success) {
        this.setState({
          errorMessage: this.props.message.content
        });
      }
    }
  }

  // check authentication before displaying form
  checkAuth() {
    app.authenticate().then((response) => {
      if (response.accessToken.length > 0) {
        // set login status to true if there is a token
        this.isLoggedIn = true;

        // set loading state to false
        this.setState({
          isLoading: false
        });
      }
    }).catch(() => {
      this.setState({
        isLoading: false
      });
    });
  }

  handleLogin() {
    // set loading to true while data is acquired from API
    this.setState({
      isLoading: true,
      errorMessage: ''
    });

    const printError = this.printError;

    // start authentication process
    app.authenticate({
      strategy: 'local',
      user: this.state.user,
      password: this.state.password
    })
      .then(response => {
        return app.passport.verifyJWT(response.accessToken);
      })
      // in case the backend is down..
      .catch(error => {
        printError(error);
      })
      .then(payload => {
        if (payload) {
          return app.service('users').get(payload.userId);
        }
      })
      .then(user => {
        if (user) {
          //encrypt it and then save to cookie
          cookieEncrypt('userId', user.id, this.app)
            .then(() => {
              app.set('user', user);
              // set login state to true
              this.isLoggedIn = true;

              this.setState({
                isLoading: false
              });
            })
        }
      })
      .catch(error => {
        printError(error);
      });
  }

  printError(error) {
    if (error.data) {
      this.setState({
        isLoading: false,
        errorMessage: error.data.message
      });
    } else {
      this.setState({
        isLoading: false,
        errorMessage: 'Error connecting to database'
      });
    }
  }

  switchForm(e) {
    e.preventDefault();
    this.setState({
      register: true
    });
  }

  handleResetPasswd(e) {
    e.preventDefault();
    this.setState({
      resetPasswd: true
    })
  }
  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { isLoggedIn, app } = this;
    const { isLoading, register, errorMessage, successMessage, resetPasswd } = this.state;

    if (isLoggedIn) {
      return (
        <div>
          {isLoading ? (
            <p>One moment please..</p>
          ) : (
              <Dashboard app={app} isLoggedIn={isLoggedIn} />
            )}
        </div>
      )
    }

    if (register) {
      return (
        <RegisterHelper app={app} />
      )
    }

    if (resetPasswd) {
      return (
        <ResetPasswd app={app} />
      )
    }

    return (
      <div className='container'>
        <div className='form-container'>
          {isLoading ? (
            <p>One moment please..</p>
          ) : (
              <div>
                <div>
                  <Form onSubmit={this.handleLogin}>
                    <Form.Field>
                      <label htmlFor="login_username">Username</label>
                      <input id="login_username" value={this.state.user} name='user' onChange={e => this.setState({ user: e.target.value })} placeholder='username' />
                    </Form.Field>
                    <Form.Field>
                      <label htmlFor="login_password">Password</label>
                      <input id="login_password" value={this.state.password} name='password' type="password" onChange={e => this.setState({ password: e.target.value })} placeholder='password' />
                    </Form.Field>
                    <Button onClick={this.props.login} type='Login'>Login</Button>
                  </Form>
                  {process.env.NODE_ENV === 'development' ? <Button type='Register' onClick={this.switchForm}>Register</Button> : ''}
                  {process.env.NODE_ENV === 'development' ? <Button type='Reset' onClick={this.handleResetPasswd}>Reset Password</Button> : ''}
                </div>
              </div>
            )}

          {successMessage &&
            // form success messages are displayed here only when there is a success message.
            <div className="success-message">
              {successMessage}
            </div>
          }

          {errorMessage &&
            // form error messages are displayed here only when there is an error message.
            <div className="error-message">
              {errorMessage}
            </div>
          }
        </div>
      </div>
    );
  }
}

export default LoginHelper;