import React, { Component } from 'react';
import { Button, Form } from 'semantic-ui-react';
import LoginHelper from './LoginHelper.jsx';


class RegisterHelper extends Component {
  constructor(props) {
    super(props);

    // grab app from the Login Component
    this.app = this.props.app;

    // bind registerUser to the this variable
    this.registerUser = this.registerUser.bind(this);

    this.customerData = this.props.customerData ? this.props.customerData : '';

    this.state = {
      user: '',
      password: '',
      email: this.customerData ? this.customerData.createData.email : '',
      registered: false,
      errorMessage: ''
    }
  }

  registerUser() {
    // get users service
    const userService = this.app.service('users');
    const profileService = this.app.service('profile');
    const checkoutService = this.app.service('checkout');

    if (this.state.user === '') {
      this.setState({
        errorMessage: 'Please provide a username'
      });
      return;
    }

    if (this.state.password === '') {
      this.setState({
        errorMessage: 'Please provide a password'
      });
      return;
    }

    // create the user
    userService.create({
      'user': this.state.user,
      'password': this.state.password,
      'email': this.state.email,
      'stripe_cust_ID': this.customerData ? this.customerData.createData.id : ''
    }).then((response) => {
      if (response) {
        this.setState({
          registered: true,
          id: response.id
        });

        //Setting up the database if coming from checkout
        //first stripe charge record
        if (this.props.customerData) {
          const custData = this.props.customerData.createData;
          const subData = this.props.customerData.subscriptionData;
          const userAgent = navigator.userAgent;
          checkoutService.create({
            'belongsTo': response.id,
            'stripe_customer_ID': custData.id,
            'name': custData.name,
            'email': custData.email,
            'description': custData.description,
            'invoice_prefix': custData.invoice_prefix,
            'discount': custData.discount,
            'default_source': custData.default_source,
            'current_period_start': subData.current_period_start,
            'current_period_end': subData.current_period_end,
            'cancel_at_period_end': subData.cancel_at_period_end,
            'plan_details': subData.plan,
            'active': subData.plan.active,
            'paid': subData.plan.amount,
            'purchased': subData.plan.product,
            'stripe_plan_ID': subData.plan.id,
            'stripe_sub_ID': subData.id,
            'userAgent': userAgent
          }).then(() => {
            //now let's create the profile to be a gold member
            profileService.create({
              'belongsTo': response.id,
              'stripe_plan_ID': subData.plan.id,
              'stripe_sub_ID': subData.id,
              'plan_starts': subData.current_period_start,
              'plan_ends': subData.current_period_end,
              'plan_status': subData.plan.active,
              'role': 'Gold'
            }).catch((err) => {
              console.log(err)
            });

          }).catch((err) => console.log(err));

        }
      }
    }).catch((err) => {
      if (err.errors) {
        this.setState({
          errorMessage: err.errors[0].message
        });
      } else {
        this.setState({
          errorMessage: err.message
        });
      }
    });
  }

  render() {
    const { registered, errorMessage } = this.state;

    if (registered) {
      return (
        <LoginHelper id={this.state.id} />
      )
    }

    return (
      <div>
        <div className="form-container">
          <Form onSubmit={this.registerUser}>
            <Form.Field>
              <label htmlFor="login-username">Username</label>
              <input id="login-username" value={this.state.user} name='user' onChange={e => this.setState({ user: e.target.value })} placeholder='username' />
            </Form.Field>
            <Form.Field>
              <label htmlFor="login-email">E-mail Address</label>
              <input id="login-email" value={this.state.email} name='email' type="email" onChange={e => this.setState({ email: e.target.value })} placeholder='email' />
            </Form.Field>
            <Form.Field>
              <label htmlFor="login-password">Password</label>
              <input id="login-password" value={this.state.password} name='password' type="password" onChange={e => this.setState({ password: e.target.value })} placeholder='password' />
            </Form.Field>
            <Button type='Login'>Register</Button>
          </Form>

          {errorMessage &&
            // form error messages are displayed here only when there is an error message.
            <div className="error-message">
              {errorMessage}
            </div>
          }
        </div>
      </div>
    );
  }
}

export default RegisterHelper;