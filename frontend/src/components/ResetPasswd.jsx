import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';
import { ifError } from 'assert';

import LoginHelper from './LoginHelper.jsx';

class ResetPasswd extends Component {
  constructor(props) {
    super(props);

    this.app = this.props.app;

    this.sendResetEmail = this.sendResetEmail.bind(this);
    this.resetPassword = this.resetPassword.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.value = this.props.match ? this.props.match.params.value : undefined;

    this.state = {
      errorMessage: '',
      successMessage: '',
      message: {}
    }
  }

  handleChange(e) {
    e.preventDefault();
    this.setState({[e.target.name]: e.target.value})
  }
  
  resetPassword(e) {
    e.preventDefault();
    const password = document.passwordForm.newPassword.value;
    const confirmPassword = document.passwordForm.confirmPassword.value;

    if(password === confirmPassword) {
      if(password === '') {
        this.setState({
          errorMessage: 'You need to enter a password first.',
          successMessage: ''
        })
      } else {
        var obj = {
          token: this.value,
          password
        }
    
        this.app.service('pwdhandler')
          .patch('', obj)
          .then(response => {
            if(response === 'success') {
              this.setState({
                errorMessage: '',
                message: {
                  content: 'Your password was successfully reset.',
                  success: true
                }
              })
            } else {
              this.setState({
                errorMessage: response,
                successMessage: ''
              })
            }

            return response;
          })
          .catch(() => {
            this.setState({
              errorMessage: 'There was a problem, please try again',
              successMessage: ''
            })
          })
      }
    } else {
      this.setState({
        errorMessage: 'Passwords do not match.',
        successMessage: ''
      })
    }
  }

  sendResetEmail(e) {
    e.preventDefault();

    const email = document.emailForm.email.value;

    this.app.service('pwdhandler')
      .create({
          action: 'sendResetPwd',
          value: {email}
      })
      .then(response => {
        if(response === 'success') {
          this.setState({
            successMessage: 'Email sent, please check your mail.',
            errorMessage: ''
          })

          return;
        }

        this.setState({
          errorMessage: 'There was a problem, please try again',
          successMessage: ''
        })
      })
      .catch(() => {
        this.setState({
          errorMessage: 'There was a problem, please try again.',
          successMessage: ''
        })
      })
  }

  render() { 
    const value = this.value;
    let {errorMessage, successMessage, message} = this.state;

    if(message.success) {
      return (
        <LoginHelper
          app={this.app}
          message={message}
        />
      )
    }

    if(value) {
      return (
        <div className='container'>
          <div className='form-container'>  
            <h2>Enter New Password</h2>

            <Form name="passwordForm" onSubmit={this.resetPassword}>
              <Form.Field>
                <label htmlFor="newPassword">Enter Password</label>
                <input id="newPassword" type="password" name="password" placeholder="enter password" />
              </Form.Field>
              <Form.Field>
                <label htmlFor="confirmPassword">Re-Confirm Password</label>
                <input id="confirmPassword" type="password" name="password" placeholder="enter password" />
                <Button>Update Password</Button>
              </Form.Field>
            </Form>

            {successMessage && 
            // form success messages are displayed here only when there is a success message.
            <div className="success-message">
              {successMessage}
            </div>
            }

            {errorMessage && 
            // form error messages are displayed here only when there is an error message.
            <div className="error-message">
              {errorMessage}
            </div>
            }

          </div>
        </div>
      )
    }

    return (
      <div className='container'>
        <div className='form-container'>  
          <h2>Reset Password</h2>

          <Form name="emailForm" onSubmit={this.sendResetEmail}>
            <Form.Field>
             <input type="email" name="email" id="email" placeholder="example@gmail.com" />
            </Form.Field>
            <Button>Send Reset Link</Button>
          </Form>

          {successMessage && 
            // form success messages are displayed here only when there is a success message.
            <div className="success-message">
              {successMessage}
            </div>
          }

          {errorMessage && 
            // form error messages are displayed here only when there is an error message.
            <div className="error-message">
              {errorMessage}
            </div>
          }
        </div>
      </div>
    );
  }
}

export default ResetPasswd;