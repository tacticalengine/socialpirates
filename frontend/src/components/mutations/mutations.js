import gql from 'graphql-tag';

//Make sure to create PROFILE through graphQL during registration

export const CREATE_PROFILE_ACTIVITY = gql `
  mutation createActivity($userId: Int!, $userIP: String!) {
    insert_profile  (objects: [{
    belongsTo: $userId,
      profileActivity: {
        data: {
          belongsTo: $userId,
          activityLogin: {
            data: {
              userID: $userId,
              userIP: $userIP
            },
            on_conflict: {
              constraint: login_info_pkey
              update_columns: [userIP]
            }
          }
        },
        on_conflict: {
          constraint: activity_belongsTo_key
          update_columns: [belongsTo]
        }
      }
    
  }], on_conflict: {
    constraint: profile_belongsTo_key
    update_columns: [belongsTo]
  }) {
    returning {
      belongsTo
      profileActivity {
        belongsTo
        activityLogin {
          userID
          userIP
        }
      }
    }
  }}`;




//update PROFILE everywhere else 
export const UPDATE_USER_INFO = gql `
    mutation updateUserInfo($name: String) {
    update_userInfo(
      where: {ownerID: {_eq: 53}},
      _set: {
        name: $name,
      }
    ) {
      affected_rows
      returning {
        ID
        ownerID
        name
      }
    }
  }`;


//Delete users 
//Keep in mind that the order is reversed when it comes to removing a user
export const DELETE_USER = gql `
mutation Deleteuser($userId: Int!) {
  delete_stripe_charge(where: {belongsTo: {_eq: $userId}}) {
    returning {
      stripe_transaction_ID
    }
  }

    delete_posting(where: {belongsTo: {_eq: $userId}}) {
    returning {
      content
    }
  }
  delete_login_info(where: {belongsTo: {_eq: $userId}}) {
    affected_rows
    returning {
      belongsTo
    }
  }
  delete_transaction_info(where: {belongsTo: {_eq: $userId}}) {
    affected_rows
    returning {
      belongsTo
    }
  }
  delete_activity(where: {belongsTo: {_eq: $userId}}) {
    affected_rows
    returning {
      belongsTo
    }
  }
  delete_profile(where: {belongsTo: {_eq: $userId}}) {
    affected_rows
    returning {
      profileID
    }
  }
  delete_users(where: {id: {_eq: $userId}}) {
    affected_rows
    returning {
      user
      id
      email
    }
  }
}

`;