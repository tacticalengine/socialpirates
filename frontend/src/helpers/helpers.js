export function slugify(text) {
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-') //replace spaces with a dash
    .replace(/[^\w\-]+/g, '') // remove all non-word characters
    .replace(/\-\-+/g, '-') //replace multiple dash with single dash

    .replace(/^-+/, '') //trim - from start of text
    .replace(/-+$/, ''); //trim - from end of text
}

export function backendURL(whichend = 'http://www.localhost:4040') {

  let theURL;
  if (whichend == 'be') {
    switch (process.env.NODE_ENV) {
      case 'development':
        theURL = process.env.BACKENDURL_LOCAL;
        break;
      case 'production':
        theURL = process.env.BACKENDURL_PROD;
        break;
      case 'testing':
        theURL = 'http://localhost:4040';
        break;
      default:
        theURL = 'http://localhost:4040';
    }
  } else if (whichend == 'fe') {
    if (process.env.NODE_ENV === 'production') {
        theURL = 'https://socialpirates-frontend.herokuapp.com/';
    } else {
        theURL = 'http://localhost:8080' ;
    }
  }


  return theURL;
}