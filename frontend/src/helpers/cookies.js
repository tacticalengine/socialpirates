export async function cookieParser(keyName, app) {
  const cookieData = document.cookie.split(';');
  const rawCookie = document.cookie.split(/=|;/).map(string => string.trim());

  let cookieContains = (rawCookie.indexOf(keyName) > -1);
  let cookieResponse;
  //only run the patch if the keyname exists in one of the cookies
  if (cookieContains) {
    await app.service('cookies')
      .patch('', {
        keyName,
        cookieData
      })
      .then(response => {
        cookieResponse = response;
      })
      .catch((err) => {
        console.log(err);
        return 'Invalid cookie';
      });

    return cookieResponse;
  } else {
    //for debugging production. will delete once figured out. this will show when logged out too
    console.log(rawCookie)
  }
}

export async function cookieEncrypt(keyName, value, app) {
  app.service('cookies')
    .create({
      keyName,
      value
    })
    .then(response => {
      document.cookie = response;
    })
    .catch(() => {
      return 'Error creating cookie';
    })
}