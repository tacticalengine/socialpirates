module.exports = function(object) {
  var out = "<ul>";

  for(var i=0, l=object.length; i<l; i++) {
    out = out + "<li>" + object[i] + "</li>";
  }

  return out + "</ul>";
}