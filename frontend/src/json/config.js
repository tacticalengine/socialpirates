
import { CookieStorage } from 'cookie-storage';
import {backendURL} from '../helpers/helpers'

let d = new Date();
d.setDate(d.getDate() + 1)
const cookieStorage = new CookieStorage({
  expires: d
});

const config = {
  'clientURL': backendURL('be'),
  'auth': {
    'header': 'Authorization',
    'prefix': '',
    'jwtStrategy': 'jwt',
    'entity': 'user',
    'service': 'users',
    'cookie': 'feathers-jwt',
    'storageKey': 'feathers-jwt',
    'storage': cookieStorage
  }
};

export default config;