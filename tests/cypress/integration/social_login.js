//cypress.cookies need to be here or else it'll clear cookies after every job. 
// we don't want that since we're testing cookies. 
Cypress.Cookies.defaults({
  whitelist: ['feathers-jwt', 'userId']
})
describe('During Login', () => {

  //testing login
  it("Logging in!", () => {
    cy.visit('localhost:8080')
    cy.get('input#login_username').click().type('pepper')
    cy.get('input#login_password').click().type('secret')
    cy.contains('Login').click()
    //The wait is for hasura in heroku to load
    cy.wait(15000)
    cy.contains('Dashboard')
  })
})

describe('In the Dashboard', () => {
  it('Are there cookies?', () => {
    cy.getCookie('feathers-jwt').should('exist')
    cy.getCookie('userId').should('exist')
  })
  it('How about settings?', () => {
    cy.contains('Settings')
  })
})

describe('Logout', () => {
  it('Let us logout', () => {
    cy.contains('Logout').click()
    cy.hash().should('be.empty')
  })

  it('The cookies better be gone or so help me', () => {
    cy.getCookie('userId').should('not.exist')
    cy.getCookie('feathers-jwt').should('not.exist')
  })
})