const Strategy = require('passport-custom');

module.exports = opts => {
  return function() {
    const verifier = async (req, done) => {

      // get the key from the request header supplied in opts
      const key = req.params.headers[opts.header];
      const userId = req.params.headers.userid;
      let match = null;

      // check if user exists before giving them authorization
      await this.service('users')
        .get(userId)
        .then(response => {
          if(response) {
            // check if the key is in the allowed keys supplied in opts
            match = opts.allowedKeys.includes(key) && userId;
          }
        })

        // user will default to false if no key is present
        // and the authorization will fail
        const user = match ? 'api' : match;

        return done(null, user, {
          userId
        });
    };

    // register the strategy in the app.passport instance
    this.passport.use('pirateAuth', new Strategy(verifier));
    // Add options for the strategy
    this.passport.options('pirateAuth', {});
  };
};