/* jshint indent: 1 */

const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const activity = sequelizeClient.define('activity', {
    activityID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    belongsTo: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      },
      unique: true
    },
    lastActivity: {
      type: DataTypes.JSON,
      allowNull: true
    },
    howManyLogins: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    profileID: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'profile',
        key: 'profileID'
      }
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: Sequelize.fn('now')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: Sequelize.fn('now')
    },
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  return activity;
};
