// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const users = sequelizeClient.define('users', {
    user: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true
      }
    },
    twitterId: {
      type: DataTypes.STRING
    },
    facebookId: {
      type: DataTypes.STRING
    },
    accessToken: {
      type: DataTypes.STRING
    },
    isVerified: {
      type: DataTypes.BOOLEAN
    },
    verifyShortToken: {
      type: DataTypes.STRING
    },
    verifyToken: {
      type: DataTypes.STRING
    },
    verifyExpires: {
      type: DataTypes.STRING
    },
    verifyChanges: {
      type: DataTypes.JSON
    },
    resetToken: {
      type: DataTypes.STRING
    },
    resetShortToken: {
      type: DataTypes.STRING
    },
    resetExpires: {
      type: DataTypes.STRING
    },
    phone: {
      type: DataTypes.STRING,
      unique: true
    },
    twofa: {
      type: DataTypes.BOOLEAN
    },
    stripe_cust_ID: {
      type: DataTypes.STRING
    },
    role: {
      type: DataTypes.STRING
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line no-unused-vars
  users.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return users;
};
