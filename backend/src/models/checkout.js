/* jshint indent: 1 */

const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const checkout = sequelizeClient.define('checkout', {
    stripe_transaction_ID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    belongsTo: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    stripe_customer_ID: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    name: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    email: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    transaction_Date: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      defaultValue: Sequelize.fn('now')
    },
    invoice_prefix: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    discount: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    default_source: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    current_period_start: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    current_period_end: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    cancel_at_period_end: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    plan_details: {
      type: DataTypes.JSONB,
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    paid: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    purchased: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    stripe_plan_ID: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    stripe_sub_ID: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    stripe_ID: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    userIP: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    userData: {
      type: DataTypes.JSON,
      allowNull: true
    },
    activityID: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'activity',
        key: 'activityID'
      }
    },
    card: {
      type: DataTypes.JSONB,
      allowNull: true
    },
    userAgent: {
      type: DataTypes.TEXT,
      allowNull: true
    },
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });
  return checkout;
};
