/* jshint indent: 1 */

const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const profile = sequelizeClient.define('profile', {
    profileID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    belongsTo: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      },
      unique: true
    },
    accessToken: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    socialID: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    socialName: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    totalPosts: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    totalProfiles: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    address: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    role: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: 'user'
    },
    displayName: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    plan_starts: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    plan_ends: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    plan_status: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    stripe_plan_ID: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    stripe_sub_ID: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    timezone: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: Sequelize.fn('now')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: Sequelize.fn('now')
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  return profile;
};
