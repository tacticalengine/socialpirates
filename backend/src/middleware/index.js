// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  // Add your custom middleware here. Remember that
  // in Express, the order matters.
  const session = require('express-session');
  const isDev = (process.env.NODE_ENV === 'development');
  if (isDev) {
    app.use(session({
      secret: 'secret treasure',
      resave: true,
      saveUnintialized: true
    }));
  }

  //sync database | To generate add tables and uncomment code | use node instead of nodemon

  // const SequelizeAuto = require('sequelize-auto');
  // const auto = new SequelizeAuto(process.env.PGDBNAME, process.env.PGUSER, process.env.PGPASS, {
  //   host: process.env.PGHOST,
  //   dialect: 'postgres',
  //   dialectOptions: {
  //     ssl: true
  //   },
  //   directory: './src/models/',
  //   pool: { max: 5, min: 0, idle: 10000 },
  //   port: process.env.PGPORT,
  //   tables: ['']
  //   });

  // auto.run(function (err) {
  //   if (err) throw err;
  //   console.log(auto.tables);
  //   console.log(auto.foreignKeys);
  // })

};