const logger = require('./logger');
const isDev = (process.env.NODE_ENV === 'development');
const app = require('./app');
const port = (isDev) ? app.get('port') : process.env.PORT || app.get('port'); 
const host = (isDev) ? app.get('host') : process.env.HOST || '0.0.0.0';
const server = app.listen(port);

process.on('unhandledRejection', (reason, p) =>
  logger.error('Unhandled Rejection at: Promise ', p, reason)
);

server.on('listening', () =>
  logger.info('Feathers application! started on http://%s:%d', host, port)
);
