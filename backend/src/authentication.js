const authentication = require('@feathersjs/authentication');
const commonHooks = require('feathers-hooks-common')
const jwt = require('@feathersjs/authentication-jwt');
const local = require('@feathersjs/authentication-local');
const oauth1 = require('@feathersjs/authentication-oauth1');
const oauth2 = require('@feathersjs/authentication-oauth2');
const Auth0Strategy = require('passport-auth0');
const FacebookStrategy = require('passport-facebook');
const TwitterStrategy = require('passport-twitter').Strategy;
require('dotenv').config();

const pirateAuth = require('./adminAuth');

module.exports = function (app) {
  const config = app.get('authentication');

  // Set up authentication with the secret
  app.configure(authentication(config));
  app.configure(jwt());
  app.configure(local());

  app.configure(
    pirateAuth({
      entity: 'user',
      service: 'users',
      // which header to look at
      header: 'x-api-key',
      // which keys are allowed
      allowedKeys: [process.env.PIRATE_APIKEY]
    })
  );

  app.configure(oauth1({
    name: 'twitter',
    entity: 'user',
    service: 'users',
    Strategy: TwitterStrategy,
    consumerKey: process.env.TWITTER_CONSUMER_KEY,
    consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
    path: '/auth/twitter',
    callbackURL: 'https://socialpirates.local/auth/twitter/callback',
  }));

  app.configure(oauth2(Object.assign({
    name: 'auth0',
    Strategy: Auth0Strategy
  }, config.auth0)));

  app.configure(oauth2(Object.assign({
    name: 'facebook',
    Strategy: FacebookStrategy,
    clientID: process.env.FACEBOOK_CLIENTID,
    clientSecret: process.env.FACEBOOK_CLIENTSECRET,
    successRedirect: "/",
    scope: [
      "public_profile",
      "email",
      "manage_pages",
      "publish_pages"
    ],
    profileFields: [
      "id",
      "displayName",
      "first_name",
      "last_name",
      "email",
      "gender",
      "profileUrl",
      "birthday",
      "picture",
      "permissions"
    ]
  })));

  // The `authentication` service is used to create a JWT.
  // The before `create` hook registers strategies that can be used
  // to create a new valid JWT (e.g. local or oauth2)
  app.service('authentication').hooks({
    before: {
      create: [
        commonHooks.iffElse(
          // if the specific header is included
          ctx => ctx.params.headers['x-api-key'],
          // authentication with this strategy
          authentication.hooks.authenticate('pirateAuth'),
          // else fallback on the jwt strategy
          authentication.hooks.authenticate(config.strategies)
        )
      ],
      remove: [
        authentication.hooks.authenticate('jwt')
      ]
    }
  });
};
