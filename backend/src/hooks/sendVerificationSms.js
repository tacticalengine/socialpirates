const accountService = require('../services/management/notifier');

const _ = () => (hook) => {
  if (!hook.params.provider) { return hook; }
  const user = hook.result;
  if (hook.data && hook.data.phone && user) {
    accountService(hook.app).notifier('resendVerifySignup', user);
    return hook;
  }
  return hook;
};

module.exports = _;