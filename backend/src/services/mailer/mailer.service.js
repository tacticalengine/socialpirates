const hooks = require('./mailer.hooks');
const Mailer = require('feathers-mailer');
const smtpTransport = require('nodemailer-smtp-transport');
require('dotenv').config();

module.exports = function (app) {
  app.use('/mailer', Mailer(smtpTransport({
    host: 'smtp-relay.sendinblue.com',
    secure: true,
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASS
    }
  })));  

  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Get our initialized service so that we can register hooks
  const service = app.service('mailer');

  service.hooks(hooks);
};
