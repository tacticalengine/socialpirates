// Initializes the `stripe/transaction` service on path `/stripe/transaction`
const { Transaction } = require('feathers-stripe');
const hooks = require('./transaction.hooks');
require('dotenv').config();

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/stripe/transaction', new Transaction({ secretKey: process.env.STRIPE_KEY }))

  // Get our initialized service so that we can register hooks
  const service = app.service('stripe/transaction');

  service.hooks(hooks);
};
