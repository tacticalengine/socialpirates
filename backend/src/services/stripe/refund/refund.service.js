// Initializes the `stripe/refund` service on path `/stripe/refund`
const { Refund } = require('feathers-stripe');
const hooks = require('./refund.hooks');
require('dotenv').config();

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/stripe/refund', new Refund({ secretKey: process.env.STRIPE_KEY }))

  // Get our initialized service so that we can register hooks
  const service = app.service('stripe/refund');

  service.hooks(hooks);
};
