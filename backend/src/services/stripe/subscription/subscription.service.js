// Initializes the `stripe/subscription` service on path `/stripe/subscription`
const { Subscription } = require('feathers-stripe');
const hooks = require('./subscription.hooks');
require('dotenv').config();

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/stripe/subscription', new Subscription({ secretKey: process.env.STRIPE_KEY }))

  // Get our initialized service so that we can register hooks
  const service = app.service('stripe/subscription');

  service.hooks(hooks);
};
