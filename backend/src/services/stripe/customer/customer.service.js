// Initializes the `stripe/customer` service on path `/stripe/customer`
const { Customer } = require('feathers-stripe');
const hooks = require('./customer.hooks');
require('dotenv').config();

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/stripe/customer', new Customer({ secretKey: process.env.STRIPE_KEY }))

  // Get our initialized service so that we can register hooks
  const service = app.service('stripe/customer');

  service.hooks(hooks);
};
