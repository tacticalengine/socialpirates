// Initializes the `stripe/charges` service on path `/stripe/charges`
const { Charge } = require('feathers-stripe');
const hooks = require('./charges.hooks');
require('dotenv').config();

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/stripe/charges', new Charge({ secretKey: process.env.STRIPE_KEY }))

  // Get our initialized service so that we can register hooks
  const service = app.service('stripe/charges');

  service.hooks(hooks);
};
