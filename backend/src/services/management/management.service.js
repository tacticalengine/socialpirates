const authManagement = require('feathers-authentication-management');
const hooks = require('./management.hooks');
const notifier = require('./notifier.js');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const identifyUserProps = app.get('identifyUserProps');
  const shortTokenLen = app.get('shortTokenLen');
  const shortTokenDigits = app.get('shortTokenDigits');

  const options = {
    paginate,
    identifyUserProps,
    shortTokenLen,
    shortTokenDigits,
  };

  // Initialize our service with any options it requires
  app.configure(authManagement(options, notifier(app)));

  // Get our initialized service so that we can register hooks
  const service = app.service('authManagement');

  service.hooks(hooks);
};
