require('dotenv').config();

const frontEndUrl = process.env.NODE_ENV === 'production' ? 'https://socialpirates-frontend.herokuapp.com' : 'http://localhost:8080';

module.exports = function(app) {
  // can be used to send SMS to user on information being changed
  function sendSms(sms) {
      return app.service('sms').create(sms).then(() => {
    }).catch((err) => {
      throw Error(err);
    });
  }

  function getLink(type, hash) {
    const url = `${frontEndUrl}/system/` + type + '/' + hash
    return url
  }

  function getResetLink(hash) {
    const url = `${frontEndUrl}/reset/${hash}`
    return url
  }

  function sendEmail(email) {
    return app.service('mailer').create(email).then(function (result) {
      console.log('Sent email', result)
    }).catch(err => {
      console.log('Error sending email', err)
    })
  }

  return {
    notifier: function(type, user, notifierOptions) {
      let tokenLink
      let email
      let code
      let sms

      switch (type) {
        case 'resendVerifySignup': //sending the user the verification email
          tokenLink = getLink('verify', user.verifyToken)
          email = {
             from: process.env.FROM_EMAIL,
             to: user.email,
             subject: 'Verify Signup',
             html: tokenLink
          }
          return sendEmail(email)
          break

        case 'verifySignup': // confirming verification
          tokenLink = getLink('verify', user.verifyToken)
          
          // set verified to true in the database
          app.service('users').patch(user.id, {
            isVerified: true
          })
          
          email = {
             from: process.env.FROM_EMAIL,
             to: user.email,
             subject: 'Confirm Signup',
             html: 'Thanks for verifying your email'
          }
          return sendEmail(email)
          break

        case 'sendResetPwd':
          tokenLink = getResetLink(user.resetToken)
          email = {
            from: process.env.FROM_EMAIL,
            to: user.email,
            subject: 'Reset Your Password',
            html: tokenLink
          }
          return sendEmail(email)
          break

        case 'resetPwdLong':
          email = {
            from: process.env.FROM_EMAIL,
            to: user.email,
            subject: 'Your password has been changed.',
            html: 'Your password was changed.'
          }
          return sendEmail(email)
          break

        case 'passwordChange':
          email = {}

          return sendEmail(email)
          break

        default:
          break
      }
    }
  }
}