// Initializes the `cookies` service on path `/cookies`
const createService = require('./cookies.class.js');
const hooks = require('./cookies.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate,
    app
  };

  // Initialize our service with any options it requires
  app.use('/cookies', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('cookies');

  service.hooks(hooks);
};
