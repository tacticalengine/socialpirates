const CryptoJS = require('crypto-js');

/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {};
  }

  setup(app) {
    this.app = app;
  }

  async create (data, params) {
    const keyName = data.keyName;
    const value = data.value;
    let cookieValue = CryptoJS.AES.encrypt(value.toString(), process.env.COOKIE_PASS);
    let d = new Date();
    d.setDate(d.getDate() + 1)
    return `${keyName}=${cookieValue};expires=${d.toUTCString()};path=/`;
  }

  async update (id, data, params) {
    return data;
  }

  async patch (id, data, params) {
    const cookieData = data.cookieData;
    const keyName = data.keyName;

    const cookie = cookieData.reduce((res, c) => {
      const [key, val] = c.trim().split('=').map(decodeURIComponent);
      try {
        return Object.assign(res, { [key]: JSON.parse(val)});
      } catch (e) {
        return Object.assign(res, { [key]: val});
      }
    }, {} );

    //check if it has it then decrypt it
    
    if (cookie.hasOwnProperty(keyName)){
      const valueOfText = cookie[keyName];
      //got the value now to decrypt
      const bytes = CryptoJS.AES.decrypt(valueOfText.toString(), process.env.COOKIE_PASS);
      return bytes.toString(CryptoJS.enc.Utf8);
    } else {
      return 'error';
    }
  }

  // async remove (id, params) {
  //   return { id };
  // }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
