// Initializes the `login` service on path `/login`
const createService = require('feathers-sequelize');
const createModel = require('../../models/login_info');
const hooks = require('./login.hooks');

module.exports = function (app) {
  const Model = createModel(app);

  const options = {
    Model
  };

  // Initialize our service with any options it requires
  app.use('/login', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('login');

  service.hooks(hooks);
};
