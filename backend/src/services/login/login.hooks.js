require('isomorphic-fetch');
const {
  authenticate
} = require('@feathersjs/authentication').hooks;

function createProfile() {

  return function (context) {
    const profileService = context.app.service('profile');
    const activityService = context.app.service('activity');
    const loginService = context.app.service('login');
    const userID = context.data.belongsTo;
    let activityID;
    let timezone;
    let profile;
    //get IP and data from ipapi
    return fetch('https://ipapi.co/json').then((response) => {
      return response.json();
    }).then((ipdata) => {
      //insert ipapi into login_info database
      timezone = ipdata.timezone;
      context.data.loginData = ipdata;
      context.data.userIP = ipdata.ip;
    }).then(_ => {
      //find profile ID that belongs to the userID 
      return profileService.find({
        query: {
          belongsTo: {
            $eq: userID
          }
        }
      }).then((profileResponse) => {
        if (profileResponse.data.length > 0) {
          //if profile exists connect profile and activity with login
          for (let x of profileResponse.data) {
            profile = x.profileID;
          }
          return loginService.find({
            query: {
              belongsTo: {
                $eq: userID
              }
            }
          }).then((loginResponse) => {
            //update profile's timezone from login
            return activityService.find({
              query: {
                belongsTo: {
                  $eq: userID
                }
              }
            }).then((activityResponse) => {
              if (activityResponse.data.length > 0) {
                for (let x of activityResponse.data) {
                  activityID = x.activityID;
                }
                return activityService.patch(activityID, {
                  belongsTo: userID,
                  profileID: profile,
                  howManyLogins: loginResponse.length
                }).catch((err) => console.log(err));
              }
            });
          });
        } else {
          return profileService.create({
            belongsTo: userID,
            timezone: timezone
          }).then((response) => {
            return activityService.create({
              belongsTo: userID,
              profileID: response.profileID
            });
          }).then((created) => {
            console.log('Profile and Activity was created for user ' + context.data.belongsTo);
          });
        }
      });
    }).then(() => {
      return activityService.find({
        query: {
          belongsTo: {
            $eq: userID
          }
        }
      }).then((activityResponse) => {
        if (activityResponse.data.length > 0) {
          for (let x of activityResponse.data) {
            activityID = x.activityID;
          }
          context.data.activityID = activityID;
        }
      });
    }).catch((err) => {
      console.log(err);
    });
  };
}

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [createProfile()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
