const crypto = require('crypto');
const bcrypt = require('bcryptjs');

const accountService = require('../management/notifier.js');

/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {};

    this.app = this.options.app;
  }

  // Used to send out reset password link to users
  async create (data, params) {  
    let returnData;

    // add a new randomly generated 14 digit code and add it to the database to do the verification against
    const resetToken = crypto.randomBytes(7).toString('hex');
    
    // timestamp for 3 hours ahead of whatever time it is at time of verification.
    const today = new Date();
    const timestamp = today.setHours(today.getHours() + 3);

    // send out email to user with link containing that code as a parameter
    await new Promise((resolve, reject) => {
      this.app.service('authManagement')
      .create(data)
      .catch(() => {
        returnData = 'failure';
        resolve();
      })
      .then(response => {
        this.app.service('users')
          .get(response.id)
          .then(response => {
            this.app.service('users')
              .patch(response.id, {
                resetToken: `${response.id}-${resetToken}`,
                resetExpires: timestamp
              })
              .then(response => {
                accountService(this.app).notifier(data.action, response);
              })
          })
          .then(() => {
            returnData = 'success';
            resolve();
          })
          .catch(err => {
            returnData = err;
            reject();
          })
      })
    }) 
      
    return returnData;
  }

  // Used to change the password of a logged in user
  async update (id, data, params) {
    let responseData;

    // Check old password against password in database
    await new Promise((resolve, reject) => {
      this.app.service('users')
      .get(id)
      .then(userData => {
        // Check if new password is the old password, if NOT throw an error.
        bcrypt.compare(data.oldPassword, userData.password)
         .then(response => {
            // IF old password matches, allow to change the password.
            if(response === true) {
              this.app.service('users')
              .patch(id, {
                password: data.password
              })
              .then(response => {
                if(response) {
                  responseData = 'success';
                
                  // Send email notification to user about password change.
                  this.app.service('mailer')
                  .create({
                    from: process.env.FROM_EMAIL,
                    to: response.email,
                    subject: 'Your password was changed.',
                    html: 'Your password was just changed, if you did not do this contact our support immediately.'
                  })

                  // FUTURE TODO: IF user has option enabled in settings, also send a SMS on Password change.
                
                  resolve();
                }
              })
            } else {
              responseData = 'old password is incorrect';
              resolve();
            }

        })
         .catch(err => {
           console.log(err);
         });
      })
    })
    

    return responseData;
  }
  
  // Used to verify code from reset password email
  async patch (id, data, params) {
    let responseMessage;

    const currentTime = new Date().getTime();
    
    if(data.token && data.token.length >= 16) {
      // receive token, separate user id from it
      const userId = data.token.split('-')[0];

      if(isNaN(userId)) {
        return 'error invalid token';
      }

      // verify that the token matches the one in the database.
      await new Promise((resolve, reject) => {
        this.app.service('users')
        .get(userId)
        .then(response => {
          if(!response) {
            responseMessage = 'error invalid token';
            resolve();
          }
          if(data.token === response.resetToken && currentTime < response.resetExpires) {
            this.app.service('users')
              // Set new password, remove tokens and expiries from database.
              .patch(userId, {
                password: data.password,
                resetToken: null,
                resetShortToken: null,
                resetExpires: null,
                verifyShortToken: null,
                verifyExpires: null
              })
              .then(response => {
                // notify user by email that their password was changed.
                this.app.service('mailer')
                  .create({
                    from: process.env.FROM_EMAIL,
                    to: response.email,
                    subject: 'Your password was changed.',
                    html: 'Your password was just changed, if you did not do this contact our support immediately.'
                  })
                  .then(response => {
                    console.log(response);
                  })
                // send message to frontend that the verification was successful.
                responseMessage = 'success';
                resolve();
              })
              .catch(err => {
                responseMessage = err;
                resolve();
              })
          } else {
            responseMessage = 'token does not match or has expired.';
            resolve();
          }
        })
      })
    } else {
      return 'error invalid token';
    }
    return responseMessage ? responseMessage : [];
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
