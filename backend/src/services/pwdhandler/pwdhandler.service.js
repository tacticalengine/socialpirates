// Initializes the `pwdhandler` service on path `/pwdhandler`
const createService = require('./pwdhandler.class.js');
const hooks = require('./pwdhandler.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate,
    app
  };

  // Initialize our service with any options it requires
  app.use('/pwdhandler', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('pwdhandler');

  service.hooks(hooks);
};
