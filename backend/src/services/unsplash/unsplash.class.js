const unsplash = require('unsplash-api');
require('dotenv').config();

unsplash.init(process.env.UNSPLASH_KEY);

/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {};
  }

  async find (params) {
    let promise = new Promise((resolve, reject) => {
      const page = params.query.page ? params.query.page : null;
      
      //Access default requested limit of photos from first requested page of search results here
      unsplash.searchPhotos(params.query.searchQuery, null, page, parseInt(params.query.limit), function(error, photos, link) {
        if(error) {
          console.log(error)
        }

        let numberOfPages = '';
        
        if(link) {
          numberOfPages= link.match(/=(.*?)&/)[1];
        }

        resolve({photos, numberOfPages});
      })
    })

    let result = await promise;

    if(result.length === 0) {
      result = "empty";
    }

    return result;
  }

  async get (id, params) {
    let promise = new Promise((resolve, reject) => {
      unsplash.getPhoto(id, params.query.width, params.query.height, null, function(error, photo) {
        resolve(photo);

        if(error) {
          console.log(error);
        }
      })
    })

    let result = await promise;

    if(result.length === 0) {
      result = "empty";
    }

    return result;
  }

  // async create (data, params) {
  //   if (Array.isArray(data)) {
  //     return Promise.all(data.map(current => this.create(current, params)));
  //   }

  //   return data;
  // }

  // async update (id, data, params) {
  //   return data;
  // }

  // async patch (id, data, params) {
  //   return data;
  // }

  // async remove (id, params) {
  //   return { id };
  // }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
