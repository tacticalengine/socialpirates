// Initializes the `unsplash` service on path `/unsplash`
const createService = require('./unsplash.class.js');
const hooks = require('./unsplash.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/unsplash', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('unsplash');

  service.hooks(hooks);
};
