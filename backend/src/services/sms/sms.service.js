// Initializes the `sms` service on path `/sms`
const hooks = require('./sms.hooks');
const smsService = require('feathers-twilio/lib').sms;

const accountSid = process.env.TWILIO_SID;
const authToken = process.env.TWILIO_TOKEN;

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate,
    accountSid,
    authToken
  };

  // Initialize our service with any options it requires
  app.use('/sms', smsService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('sms');

  service.hooks(hooks);
};
