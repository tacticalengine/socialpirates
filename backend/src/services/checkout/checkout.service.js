// Initializes the `stripe-charge` service on path `/stripe-charge`
const createService = require('feathers-sequelize');
const createModel = require('../../models/checkout');
const hooks = require('./checkout.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  
  const paginate = app.get('paginate');

  const options = {
    Model,
  };

  // Initialize our service with any options it requires
  app.use('/checkout', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('checkout');

  service.hooks(hooks);
};
