const {
  authenticate
} = require('@feathersjs/authentication').hooks;
require('isomorphic-fetch');

function collectData() {
  return function (context) {
    return fetch('https://ipapi.co/json').then((response) => {
      return response.json();
    }).then((ipdata) => {
      context.data.userIP = ipdata.ip;
      context.data.userData = ipdata;
    });
  };
}

module.exports = {
  before: {
    all: [],
    find: [ authenticate('jwt') ],
    get: [ authenticate('jwt') ],
    create: [collectData()],
    update: [ authenticate('jwt') ],
    patch: [ authenticate('jwt') ],
    remove: [ authenticate('jwt') ]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
