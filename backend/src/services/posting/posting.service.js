// Initializes the `posting` service on path `/posting`
const createService = require('feathers-sequelize');
const createModel = require('../../models/posting.js');
const hooks = require('./posting.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate,
    app
  };

  // Initialize our service with any options it requires
  app.use('/posting', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('posting');

  service.hooks(hooks);
};
