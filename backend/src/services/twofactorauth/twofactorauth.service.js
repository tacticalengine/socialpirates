// Initializes the `twofactorauth` service on path `/twofactorauth`
const createService = require('./twofactorauth.class.js');
const hooks = require('./twofactorauth.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate,
    app
  };

  // Initialize our service with any options it requires
  app.use('/twofactorauth', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('twofactorauth');

  service.hooks(hooks);
};
