require('dotenv').config();

/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {};

    this.app = this.options.app;
  }

  async create (data, params) {
    // Create a six digit code to send to a phone for verification
    const code = Math.floor(100000 + Math.random() * 900000);
   
    // Add six digit code to the database along with an expiry date/time for 3 hours
    const today = new Date();

    // timestamp for 3 hours ahead of whatever time it is at time of verification.
    const timestamp = today.setHours(today.getHours() + 3);

    await this.app.service('users')
      .patch(data.userId, {
        "verifyShortToken": code,
        "verifyExpires": timestamp
      }).then((response) => {
        if(response) {
          this.returnMessage = 'success';
        }       
      }).catch(err => {
        if(err) {
          this.returnMessage = err.message;
          return;
        }
      }).then(() => {
        // Send out SMS to user
        this.app.service('sms').create({
          from: process.env.TWILIO_NUMBER,
          to: data.phone,
          body: `Here is your code: ${code}`
        }).catch(err => {
          console.log(err);
        })
      })
      
      // Send back error/success message so user can enter verification code on the frontend
      return this.returnMessage;
  }


  async patch(id, data, params) {
    // Receive the six digit code.
    const vcode = data.vcode; 

    if(vcode.length === 6) {
      // Compare it to the code in the database, check if the expiry on the code is still valid.
      await this.app.service('users')
      .get(id)
      .then(response => {
        var today = new Date();

        // timestamp of current time when code was sent in.
        var timeNow = today.setHours(today.getHours());

        if(timeNow < response.verifyExpires) {
          if(vcode === response.verifyShortToken) {
            this.returnMessage = 'success';
            // If success set 2fa column in users table to `true`. 
            this.app.service('users')
              .patch(id, {
                twofa: true
              })
          } else {
            this.returnMessage = 'failed';
          }
        }
      })
      .catch(err => {
        console.log(err);
        this.returnMessage = 'failed';
      })
    } else {
      this.returnMessage = 'failed';
    }

    // Return message to user regarding their validation, if success or failure.
    return this.returnMessage;
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
