const Client = require('node-pexels').Client;

const client = new Client(process.env.PEXELS_KEY);

/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {};
  }

  async find (params) {
    let returnData;

    await client.search(params.query.searchQuery, params.query.limit, params.query.page)
    .then((results) => {
      // Do something with results
      returnData = results;
    })
    .catch((error) => {
      // Something bad happened
      returnData = error;
    });

    return returnData
  }

  // async get (id, params) {
  //   return {
  //     id, text: `A new message with ID: ${id}!`
  //   };
  // }

  // async create (data, params) {
  //   if (Array.isArray(data)) {
  //     return Promise.all(data.map(current => this.create(current, params)));
  //   }

  //   return data;
  // }

  // async update (id, data, params) {
  //   return data;
  // }

  // async patch (id, data, params) {
  //   return data;
  // }

  // async remove (id, params) {
  //   return { id };
  // }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
