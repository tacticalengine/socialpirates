const users = require('./users/users.service.js');
const mailer = require('./mailer/mailer.service.js');
const management = require('./management/management.service.js');
const sms = require('./sms/sms.service.js');
const twofactorauth = require('./twofactorauth/twofactorauth.service.js');
const stripeCharges = require('./stripe/charges/charges.service.js');
const handler = require('./handler/handler.service.js');
const stripeCustomer = require('./stripe/customer/customer.service.js');
const stripeSubscription = require('./stripe/subscription/subscription.service.js');

const stripeTransaction = require('./stripe/transaction/transaction.service.js');

const pwdhandler = require('./pwdhandler/pwdhandler.service.js');

const posting = require('./posting/posting.service.js');
const stripeRefund = require('./stripe/refund/refund.service.js');

const login = require('./login/login.service.js');

const activity = require('./activity/activity.service.js');
const cookies = require('./cookies/cookies.service.js');

const profile = require('./profile/profile.service.js');

const pexels = require('./pexels/pexels.service.js');

const unsplash = require('./unsplash/unsplash.service.js');


const checkout = require('./checkout/checkout.service.js');


// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(mailer);
  app.configure(sms);
  app.configure(management);
  app.configure(twofactorauth);
  app.configure(stripeCharges);
  app.configure(handler);
  app.configure(stripeCustomer);
  app.configure(stripeSubscription);
  app.configure(stripeTransaction);
  app.configure(pwdhandler);
  app.configure(posting);
  app.configure(stripeRefund);
  app.configure(login);
  app.configure(activity);
  app.configure(cookies);
  app.configure(profile);
  app.configure(pexels);
  app.configure(unsplash);
  app.configure(checkout);
};
