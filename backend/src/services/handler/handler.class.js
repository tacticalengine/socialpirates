/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {};

    this.app = this.options.app;
  }

  // this is used to send back billing history
  async find (params) {
    const customerId = params.query.customerId;
    let billingData;
    
    await this.app.service('stripe/charges')
      .find({
        query: {
          limit: 20,
          customer: customerId
        } 
      })
      .then(response => {
        billingData = response;
      })

    return billingData;
  }

  // currently empty
  async get (id, params) {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  // creates a customer, subscription and charges customer at stripe before registration.
  async create (data, params) {
    let createData;
    let subscriptionData;

    const plan = data.plan;

    if(data.token) { 
      const token = data.token;

      const customerInfo = {
        email: token.email,
        source: token.id
      }

      // create customer account at Stripe
      await new Promise((resolve, reject) => {
        this.app.service('stripe/customer')
          .create(customerInfo)
          .then(response => {
            createData = response;
            this.app.service('stripe/subscription')
              .create({
                customer: response.id,
                items: [
                  {
                    plan
                  }
                ]
              })
              .then(response => {
                subscriptionData = response;
                resolve();
              })
              .catch(err => {
                console.log(err);
                reject();
              })
          })
          .catch(error => {
            createData = error;
          })
      })
    }

    return {createData, subscriptionData};
  }

  // is used to issue refunds to users
  async update (id, data, params) {
    let responseData;

    await this.app.service('stripe/refund')
      .create({
        charge: data.chargeId
      })
      .then(response => {
        responseData = response;
      })

    return responseData;
  }

  // is currently empty
  // async patch (id, data, params) {

  // }

  // currently empty
  // async remove (id, params) {
  //   return { id };
  // }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
