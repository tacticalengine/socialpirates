const { authenticate } = require('@feathersjs/authentication').hooks;

// TODO: secure update method so its only accessible by admins.

module.exports = {
  before: {
    all: [],
    find: [ authenticate('jwt') ],
    get: [],
    create: [],
    update: [ authenticate('jwt') ],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      function(context) {
        if(context.error) {
          console.log(context.error.message);
        }
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
