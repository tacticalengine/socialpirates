// Initializes the `handler` service on path `/handler`
const createService = require('./handler.class.js');
const hooks = require('./handler.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate,
    app
  };

  // Initialize our service with any options it requires
  app.use('/handler', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('handler');

  service.hooks(hooks);
};
