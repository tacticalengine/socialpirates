const { authenticate } = require('@feathersjs/authentication').hooks;
const accountService = require('../management/notifier.js');
const verifyHooks = require('feathers-authentication-management').hooks;
const commonHooks = require('feathers-hooks-common');

const {
  hashPassword, protect
} = require('@feathersjs/authentication-local').hooks;

function customizeDataBeforeCreatingAccount() {
  return function(context) {
    // If it is a twitter user
    if (context.data.twitter) {
      context.data.email = context.data.twitter.email; 
      context.data.user = context.data.twitter.profile.username;
      context.data.password = context.data.twitter.accessToken;
      context.data.accessToken = context.data.twitter.accessToken;
    }

    // If its a facebook user
    if (context.data.facebook) {
      context.data.email = context.data.facebook.profile._json.email; 
      context.data.user = context.data.facebook.profile.name.givenName.concat(context.data.facebook.profile.name.familyName);
      context.data.password = context.data.facebook.accessToken;
      context.data.accessToken = context.data.facebook.accessToken;
    }

    // If you want to do something whenever any OAuth
    // provider authentication occurs you can do this.
    if (context.params.oauth) {
      // do something for all OAuth providers
    }

    return Promise.resolve(context);
  };
}

function checkUserBeforeUpdate() {
  return function(context) {
    if(context.params.user) {
      if(parseInt(context.arguments[0]) !== context.params.user.id) {
        throw new Error('Users can edit or view their own information only.');
      }
    } 
    
    if(context.app && context.id) {
      if(context.arguments[0] !== context.id) {
        throw new Error('Users can edit or view their own information only.');
      }
    };
  }
}


module.exports = {
  before: {
    all: [],
    find: [ authenticate('jwt') ],
    get: [ 
      authenticate('jwt')
    ],
    create: [ 
      customizeDataBeforeCreatingAccount(),
      hashPassword(),
      verifyHooks.addVerification(),
    ],
    update: [ 
      commonHooks.disallow('external'),
      customizeDataBeforeCreatingAccount(),
      hashPassword(),  
      authenticate('jwt'),
      checkUserBeforeUpdate()
    ],
    patch: [
      commonHooks.iff(
        commonHooks.isProvider('external'),
          commonHooks.preventChanges(true, 
            'email',
            'password',
            'isVerified',
            'verifyToken',
            'verifyExpires',
            'verifyChanges',
            'resetToken',
            'resetExpires'
          ), 
      ),
      hashPassword(),  
      authenticate('jwt'),
      checkUserBeforeUpdate()
     ],
    remove: [ 
      authenticate('jwt'),
      checkUserBeforeUpdate()
    ]
  },

  after: {
    all: [ 
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [],
    get: [],
    create: [
      context => {
        accountService(context.app).notifier('resendVerifySignup', context.result)
      },
      verifyHooks.removeVerification()
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      function(context) {
        if(context.error) {
          console.log(context.error.message);
        }
      }
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};