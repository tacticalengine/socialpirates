const assert = require('assert');
const app = require('../../src/app');

describe('\'handler\' service', () => {
  it('registered the service', () => {
    const service = app.service('handler');

    assert.ok(service, 'Registered the service');
  });
});
