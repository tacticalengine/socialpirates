const assert = require('assert');
const app = require('../../src/app');

describe('\'pwdhandler\' service', () => {
  it('registered the service', () => {
    const service = app.service('pwdhandler');

    assert.ok(service, 'Registered the service');
  });
});
