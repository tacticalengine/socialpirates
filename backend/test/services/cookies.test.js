const assert = require('assert');
const app = require('../../src/app');

describe('\'cookies\' service', () => {
  it('registered the service', () => {
    const service = app.service('cookies');

    assert.ok(service, 'Registered the service');
  });
});
