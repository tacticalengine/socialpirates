const assert = require('assert');
const app = require('../../src/app');

describe('\'unsplash\' service', () => {
  it('registered the service', () => {
    const service = app.service('unsplash');

    assert.ok(service, 'Registered the service');
  });
});
