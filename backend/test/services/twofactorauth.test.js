const assert = require('assert');
const app = require('../../src/app');

describe('\'twofactorauth\' service', () => {
  it('registered the service', () => {
    const service = app.service('twofactorauth');

    assert.ok(service, 'Registered the service');
  });
});
