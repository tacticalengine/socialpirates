const assert = require('assert');
const app = require('../../src/app');

describe('\'posting\' service', () => {
  it('registered the service', () => {
    const service = app.service('posting');

    assert.ok(service, 'Registered the service');
  });
});
