const assert = require('assert');
const app = require('../../../src/app');

describe('\'stripe/transaction\' service', () => {
  it('registered the service', () => {
    const service = app.service('stripe/transaction');

    assert.ok(service, 'Registered the service');
  });
});
