const assert = require('assert');
const app = require('../../../src/app');

describe('\'stripe/charges\' service', () => {
  it('registered the service', () => {
    const service = app.service('stripe/charges');

    assert.ok(service, 'Registered the service');
  });
});
