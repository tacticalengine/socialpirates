const assert = require('assert');
const app = require('../../../src/app');

describe('\'stripe/subscription\' service', () => {
  it('registered the service', () => {
    const service = app.service('stripe/subscription');

    assert.ok(service, 'Registered the service');
  });
});
