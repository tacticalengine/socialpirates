const assert = require('assert');
const app = require('../../../src/app');

describe('\'stripe/customer\' service', () => {
  it('registered the service', () => {
    const service = app.service('stripe/customer');

    assert.ok(service, 'Registered the service');
  });
});
