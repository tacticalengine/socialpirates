const assert = require('assert');
const app = require('../../../src/app');

describe('\'stripe/refund\' service', () => {
  it('registered the service', () => {
    const service = app.service('stripe/refund');

    assert.ok(service, 'Registered the service');
  });
});
