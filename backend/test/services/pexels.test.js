const assert = require('assert');
const app = require('../../src/app');

describe('\'pexels\' service', () => {
  it('registered the service', () => {
    const service = app.service('pexels');

    assert.ok(service, 'Registered the service');
  });
});
